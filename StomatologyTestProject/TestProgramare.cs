﻿using NUnit.Framework;
using StomatologyApp;

namespace StomatologyTestProject
{
	public class TestProgramare
	{
		private readonly Programare prog1 = new Programare();

		[SetUp]
		public void SetUp()
		{
			Date programat = new Date(5, Luna.Ianuarie, 2019);
			Date efectuat = new Date(5, Luna.Ianuarie, 2019);

			prog1.SetProgramat(programat);
			prog1.SetEfectuat(efectuat);
		}

		[Test]
		public void Test1()
		{
			System.Console.WriteLine(prog1.ToString());
			string repr = "Programat la: 5/01/2019  Efectuat la: 5/01/2019";
			Assert.AreEqual(prog1.ToString(), repr);
		}
	}
}

using NUnit.Framework;
using StomatologyApp;
using System;

namespace StomatologyTestProject
{
	class TestDetaliiPersonale
	{
		private readonly DetaliiPersonale detaliiPersonale = new DetaliiPersonale();

		[SetUp]
		public void Setup()
		{
			detaliiPersonale.SetNume("Paunescu");
			detaliiPersonale.SetPrenume("Andrei");
			detaliiPersonale.SetCNP("5001209374522");
			detaliiPersonale.SetNrTelefon("07845331420");
		}

		[Test]
		public void Test1()
		{
			string repr = "Paunescu Andrei 5001209374522 07845331420";
			Console.WriteLine(detaliiPersonale.ToString());
			Assert.AreEqual(detaliiPersonale.ToString(), repr);
		}
	}
}
﻿using NUnit.Framework;
using StomatologyApp;

namespace StomatologyTestProject
{
	public class TestAdresaDomiciliu
	{
		private readonly AdresaDomiciliu adresaDomiciliu = new AdresaDomiciliu();

		[SetUp]
		public void Setup()
		{
			adresaDomiciliu.SetStrada("Stefan cel Mare");
			adresaDomiciliu.SetBloc(312);
			adresaDomiciliu.SetScara('A');
			adresaDomiciliu.SetEtaj(1);
			adresaDomiciliu.SetApartament(3);
		}

		[Test]
		public void Test1()
		{
			Assert.AreEqual(adresaDomiciliu.GetStrada(), "Stefan cel Mare");
			Assert.AreEqual(adresaDomiciliu.GetScara(), 'A');
		}
	}
}

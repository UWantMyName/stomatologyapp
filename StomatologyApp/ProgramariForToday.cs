﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StomatologyApp
{
	public partial class ProgramariForToday : Form
	{
		private List<string> pathsPacientiProgramareToday = new List<string>();

		public ProgramariForToday()
		{
			InitializeComponent();
			CheckForPacienti();
			AddButtons();
		}

		private void CheckForPacienti()
		{
			string dateOfDocument = "";

			if (!File.Exists(Directory.GetCurrentDirectory() + "/Pac2Day.stapp")) LoadPacienti();
			else
			{
				using (StreamReader read = new StreamReader(Directory.GetCurrentDirectory() + "/Pac2Day.stapp"))
				{
					dateOfDocument = read.ReadLine();
					read.Close();
				}

				if (DifferentDates(dateOfDocument)) LoadPacienti();
				else ReadPacienti();
			}
		}

		private bool DifferentDates(string date)
		{
			string[] parts = date.Split(' ');
			Date documentDate = new Date(parts[0], parts[1], parts[2]);
			Date currentDate = new Date(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

			if (documentDate.GetZi() == currentDate.GetZi() && documentDate.GetLuna() == currentDate.GetLuna() &&
				documentDate.GetAn() == currentDate.GetAn())
				return false;
			return true;
		}

		private void LoadPacienti()
		{
			// get the path for the pacienti list
			// look in the list
			// see the programari from the latest to oldest
			// grab the programari with the date of today
			// put them in the list
			// and transfer them whenever necessary.

			try
			{
				string folderNameToLookFor = "";
				Date dt = new Date(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
				folderNameToLookFor = dt.ToString();

				progressBar1.Value = 10;

				// get the path for the pacienti list
				string pacienti_path = "";
				using (StreamReader read = new StreamReader(Directory.GetCurrentDirectory() + "/path.stapp"))
				{
					pacienti_path = read.ReadLine();
					read.Close();
				}

				progressBar1.Value = 20;

				// look in the list
				string[] pacienti = Directory.GetDirectories(pacienti_path);
				progressBar1.Value = 30;
				foreach (string pacientFolder in pacienti)
				{
					// see the programari from the latest to oldest
					// grab the programari with the date of today
					IEnumerable<string> programariDeAzi =
						from programare in Directory.GetDirectories(pacientFolder)
						where programare.Contains(folderNameToLookFor)
						select programare;

					// put them in the list
					if (programariDeAzi.Count() != 0)
						foreach (string s in programariDeAzi)
							pathsPacientiProgramareToday.Add(s);
				}

				progressBar1.Value = 70;

				// and save them in the specified path
				if (File.Exists(Directory.GetCurrentDirectory() + "/Pac2Day.stapp"))
					File.Delete(Directory.GetCurrentDirectory() + "/Pac2Day.stapp");
				progressBar1.Value = 80;
				using (StreamWriter write = new StreamWriter(Directory.GetCurrentDirectory() + "/Pac2Day.stapp"))
				{
					write.WriteLine(dt.ToString());
					foreach (string s in pathsPacientiProgramareToday)
						write.WriteLine(s);
					write.Close();
				}
				progressBar1.Value = 100;
			}
			catch (DirectoryNotFoundException exc)
			{
				MessageBox.Show("Nu pot accesa calea pentru salvarea datelor! Probabil un hard-disk extern sau stick USB a fost decuplat din greseala?", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
				throw exc;
			}
			catch (Exception e)
			{
				MessageBox.Show("Eroare neasteptata. Raporteaza eroarea urmatoare" + (Environment.NewLine + Environment.NewLine) + e.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
				throw e;
			}
		}

		private void ReadPacienti()
		{
			using (StreamReader read = new StreamReader(Directory.GetCurrentDirectory() + "/Pac2Day.stapp"))
			{
				read.ReadLine();
				string line = read.ReadLine();
				while (line != null)
				{
					pathsPacientiProgramareToday.Add(line);
					line = read.ReadLine();
				}
				read.Close();
			}
		}

		private void AddButtons()
		{
			int tb = 0;

			foreach (string s in pathsPacientiProgramareToday)
			{
				tb++;
				string[] parts = s.Split('\\');
				Button b = new Button();


				b.Name = "Pacient " + tb.ToString();
				b.Cursor = Cursors.Hand;
				b.Font = new Font("Rockwell", 12);
				b.Size = new Size(417, 42);
				b.Text = parts[parts.Length - 2];
				b.TabIndex = tb;
				b.Visible = true;
				b.Click += new EventHandler(PacientButton_Click);

				flowLayoutPanel1.Controls.Add(b);
			}
		}

		private void PacientButton_Click(object sender, EventArgs e)
		{
			// I need to read the DatePersonale.stapp from the folders[button.tabindex - 1]
			Button b = sender as Button;
			string pacientFolder = "";
			for (int i = 0; i < pathsPacientiProgramareToday[b.TabIndex - 1].LastIndexOf('\\'); i++)
				pacientFolder += pathsPacientiProgramareToday[b.TabIndex - 1][i];
			string path = pacientFolder + "\\" + "DatePersonale.stapp";
			Pacient p = new Pacient();

			using (StreamReader read = new StreamReader(path))
			{
				// nume
				string line = read.ReadLine();
				string nume = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					nume += line[i];
				p.SetNume(nume);

				// prenume
				line = read.ReadLine();
				string prenume = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					prenume += line[i];
				p.SetPrenume(prenume);

				// CNP
				line = read.ReadLine();
				string CNP = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					CNP += line[i];
				p.SetCNP(CNP);

				// numar telefon
				line = read.ReadLine();
				string nrtelefon = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					nrtelefon += line[i];
				p.SetNrTelefon(nrtelefon);

				// adresa email
				line = read.ReadLine();
				string email = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					email += line[i];
				p.SetEmail(email);

				// sexul
				line = read.ReadLine();
				string sex = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					sex += line[i];
				p.SetSex(sex);

				// skip one line
				read.ReadLine();

				// adresa domiciliu
				line = read.ReadLine();
				string adresa = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					adresa += line[i];

				line = read.ReadLine();
				while (!line.Contains(":"))
				{
					adresa += Environment.NewLine;
					adresa += line;
					line = read.ReadLine();
				}
				p.SetAdresa(adresa);

				// localitate
				// line is already read from the previous section
				string localitate = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					localitate += line[i];
				p.SetLocalitate(localitate);

				// judet
				line = read.ReadLine();
				string judetul = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					judetul += line[i];
				p.SetJudetul(judetul);

				read.Close();
			}


			DetaliiPacientSelectat f = new DetaliiPacientSelectat(p, pacientFolder);
			f.Show();
			this.Close();
		}
	}
}

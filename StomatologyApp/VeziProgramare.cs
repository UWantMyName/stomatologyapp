﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StomatologyApp
{
	public partial class VeziProgramare : Form
	{
		private bool changed = false;
		private bool salveaza_pressed = false;
		private string[] files;
		private Pacient pacient;
		private Programare programare;
		private string prog_path;

		public VeziProgramare(Pacient pacient, Programare prog, string path)
		{
			InitializeComponent();
			this.pacient = pacient;
			this.programare = prog;
			this.prog_path = path;
			InitForm(prog);
			InitRadiografii(path);
		}

		private void InitForm(Programare prog)
		{
			Data_TextBox.Text = prog.GetDataProgramat().ToString();
			Ora_TextBox.Text = prog.GetOraProgramat().ToString();
			Diagnostic_TB.Text = prog.GetDiagnostic();
			foreach (string s in prog.GetDintiAfectati())
			{
				Dinti_TB.Text += s;
				Dinti_TB.Text += Environment.NewLine;
			}

			int total = 0;
			List<string> tratamenteNume = prog.GetTratamenteNume();
			List<int> tratamentePreturi = prog.GetTratamentePreturi();
			for (int i = 0; i < tratamenteNume.Count; i++)
			{
				total += tratamentePreturi[i];
				Tratament_TB.Text += tratamenteNume[i] + " --- " + tratamentePreturi[i];
				Tratament_TB.Text += Environment.NewLine;
			}

			Total_TB.Text = total.ToString();

			if (prog.GetPlatit())
			{
				Platit_RB.Checked = true;
				Platit_RB.Enabled = false;
				DataPlatit.Visible = true;
				DataPlatit.Enabled = false;
				label3.Visible = true;
				Salveaza_Button.Visible = false;
			}

			if (prog.GetPrezentat() == "DA")
			{
				PacientPrezentat_CB.SelectedIndex = 0;
				PacientPrezentat_CB.Enabled = false;
			}
			else if (prog.GetPrezentat() == "NU")
			{
				PacientPrezentat_CB.SelectedIndex = 1;
				PacientPrezentat_CB.Enabled = false;
			}
		}

		private void InitRadiografii(string path)
		{
			string rad_path = path + "\\" + "Radiografii";
			files = Directory.GetFiles(rad_path);
			int tab_index = 1;
			foreach (string s in files)
			{
				string[] parts = s.Split('\\');
				Button b = new Button();
				b.Name = "button" + tab_index.ToString();
				b.Text = parts[parts.Length - 1];
				b.Cursor = Cursors.Hand;
				b.Font = new Font("Rockwell", 12);
				b.Visible = true;
				b.BackColor = Color.White;
				b.ForeColor = Color.DeepSkyBlue;
				b.Size = new Size(393, 31);
				b.Click += new EventHandler(RadiografieButton_Click);
				buttonsPanel.Controls.Add(b);
			}
		}

		private void RadiografieButton_Click(object sender, EventArgs e)
		{
			Button b = sender as Button;
			System.Diagnostics.Process.Start(files[b.TabIndex]);
		}

		private void Platit_RB_CheckedChanged(object sender, EventArgs e)
		{
			changed = true;
			if (Platit_RB.Checked == true)
			{
				label3.Visible = true;
				DataPlatit.Visible = true;
				Platit_RB.ForeColor = Color.LimeGreen;
			}
			else
			{
				label3.Visible = true;
				DataPlatit.Visible = true;
				Platit_RB.ForeColor = Color.Red;
			}
		}

		[Obsolete]
		private void Salveaza_Button_Click(object sender, EventArgs e)
		{
			salveaza_pressed = true;
			// the paid field should be the only thing that needs to be saved, really
			if (Platit_RB.Checked)
			{
				programare.SetPlatit(true);
				programare.SetDataPlatit(DataPlatit.Value);
			}
			else
			{
				programare.SetPlatit(false);
			}

			if (PacientPrezentat_CB.SelectedIndex == 0) programare.SetPrezentat("DA");
			else if (PacientPrezentat_CB.SelectedIndex == 1) programare.SetPrezentat("NU");

			try
			{
				if (!changed)
				{
					MessageBox.Show("Nicio salvare efectuata. Formularul se va inchide.");
					this.Close();
				}
				else
				{
					DataManagement.UpdatePlataDeviz(pacient, programare, prog_path);
					MessageBox.Show("Programare actualizata!");
					this.Close();
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show("O eroare a avut loc. Nu s-a efectuat nimic.");
			}
		}

		private void PacientPrezentat_CB_SelectedIndexChanged(object sender, EventArgs e)
		{
			changed = true;
			programare.SetPrezentat((string)PacientPrezentat_CB.SelectedItem);
		}

		private void VeziProgramare_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!salveaza_pressed && changed && (Platit_RB.Enabled == true || PacientPrezentat_CB.Enabled == true || DataPlatit.Enabled == true))
			{
				string message = "S-au facut modificari. Iesi fara sa salvezi?";
				string title = "Atentie!";
				MessageBoxButtons buttons = MessageBoxButtons.YesNo;
				DialogResult result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);

				if (result == DialogResult.Yes) e.Cancel = false;
				else e.Cancel = true;
			}
		}

		private void DataPlatit_ValueChanged(object sender, EventArgs e)
		{
			changed = true;
		}
	}
}

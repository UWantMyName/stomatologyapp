﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StomatologyApp
{
	class ButtonPair
	{
		private readonly Button ChooseFileButton;
		private readonly Button DeleteFileButton;

		public ButtonPair(Button b1, Button b2)
		{
			this.ChooseFileButton = b1;
			this.DeleteFileButton = b2;
		}

		public Button GetChooseFileButton() { return this.ChooseFileButton; }
		public Button GetDeleteFileButton() { return this.DeleteFileButton; }
		public bool Exists(string name)
		{
			if (ChooseFileButton.Name == name) return true;
			if (DeleteFileButton.Name == name) return true;
			return false;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace StomatologyApp
{
	static class Tratamente
	{
		private static Dictionary<string, int> preturi = new Dictionary<string, int>();

		public static void InitPreturi()
		{
			if (File.Exists(Directory.GetCurrentDirectory() + "/price.stapp"))
			{
				using (StreamReader read = new StreamReader(Directory.GetCurrentDirectory() + "/price.stapp"))
				{
					string line = "";

					for (int i = 0; i < File.ReadAllLines(Directory.GetCurrentDirectory() + "/price.stapp").Length; i++)
					{
						line = read.ReadLine();
						string[] words = line.Split(' ');

						string name = "";
						int price = 0;

						foreach (string s in words)
							if ('0' <= s[0] && s[0] <= '9') price = int.Parse(s);
							else if (('a' <= s[0] && s[0] <= 'z') || ('A' <= s[0] && s[0] <= 'Z'))
							{
								name += s;
								name += " ";
							}

						if (!preturi.ContainsKey(name)) preturi.Add(name, price);
					}
				}
			}
			else
			{
				// I will have to create the file and populate it with old data.
				using (StreamWriter write = new StreamWriter(Directory.GetCurrentDirectory() + "/price.stapp"))
				{
					write.WriteLine("Amprenta 100");
					preturi.Add("Amprenta", 100);
					write.WriteLine("Proba schelet metalic 0");
					preturi.Add("Proba schelet metalic", 0);
					write.WriteLine("Proba schelet dentina 0");
					preturi.Add("Proba schelet dentina", 0);
					write.WriteLine("Adaptare primara proteza 0");
					preturi.Add("Adaptare primara proteza", 0);
					write.WriteLine("Adaptare primara lucrare protetica 0");
					preturi.Add("Adaptare primara lucrare protetica", 0);
					write.WriteLine("Reparatii proteza 100");
					preturi.Add("Reparatii proteza", 100);
					write.WriteLine("Adaugat dinte pe proteza 50");
					preturi.Add("Adaugat dinte pe proteza", 50);
					write.WriteLine("Captusire proteza 150");
					preturi.Add("Captusire proteza", 150);
					write.WriteLine("Etapa intermediara gangrena 0");
					preturi.Add("Etapa intermediara gangrena", 0);
					write.WriteLine("Consultatie 150");
					preturi.Add("Consultatie", 150);
					write.WriteLine("Pansament Calmant 150");
					preturi.Add("Pansament Calmant", 150);
					write.WriteLine("Drenaj Endodontic 150");
					preturi.Add("Drenaj Endodontic", 150);
					write.WriteLine("Detartraj Manual 150");
					preturi.Add("Detartraj Manual", 150);
					write.WriteLine("Detartraj Ultrasonic 200");
					preturi.Add("Detartraj Ultrasonic", 200);
					write.WriteLine("Periaj Profesional 150");
					preturi.Add("Periaj Profesional", 150);
					write.WriteLine("Obturatie Compozit Fotopolimerizabil 200");
					preturi.Add("Obturatie Compozit Fotopolimerizabil", 200);
					write.WriteLine("Indepartare Lucrare 50");
					preturi.Add("Indepartare Lucrare", 50);
					write.WriteLine("Pulpectomie Dinte Monoradicular 350");
					preturi.Add("Pulpectomie Dinte Monoradicular", 350);
					write.WriteLine("Pulpectomie Dinte Pluriradicular 400");
					preturi.Add("Pulpectomie Dinte Pluriradicular", 400);
					write.WriteLine("Tratament Gangrena Monoradicular 400");
					preturi.Add("Tratament Gangrena Monoradicular", 400);
					write.WriteLine("Tratament Gangrena Pluriradicular 450");
					preturi.Add("Tratament Gangrena Pluriradicular", 450);
					write.WriteLine("Reconstituire dupa Pulpectomie la Dinte Monoradicular 200");
					preturi.Add("Reconstituire dupa Pulpectomie la Dinte Monoradicular", 200);
					write.WriteLine("Reconstituire dupa Pulpectomie la Dinte Pluriradicular 200");
					preturi.Add("Reconstituire dupa Pulpectomie la Dinte Pluriradicular", 200);
					write.WriteLine("Reconstituire cu Dentatus si Compozit Fotopolimerizabil 250");
					preturi.Add("Reconstituire cu Dentatus si Compozit Fotopolimerizabil", 250);
					write.WriteLine("Dezobturat Canal 50");
					preturi.Add("Dezobturat Canal", 50);
					write.WriteLine("Coroana Turnata 280");
					preturi.Add("Coroana Turnata", 280);
					write.WriteLine("Coroana Metalo-Ceramica Semifizionomica 400");
					preturi.Add("Coroana Metalo-Ceramica Semifizionomica", 400);
					write.WriteLine("Coroana Metalo-Ceramica Total Fizionomica 500");
					preturi.Add("Coroana Metalo-Ceramica Total Fizionomica", 500);
					write.WriteLine("Coroana Zirconiu 700");
					preturi.Add("Coroana Zirconiu", 700);
					write.WriteLine("Coroana Ceramica pe Zirconiu Semifizionimica 800");
					preturi.Add("Coroana Ceramica pe Zirconiu Semifizionimica", 800);
					write.WriteLine("Coroana Ceramica pe Zirconiu Total Fizionimica 900");
					preturi.Add("Coroana Ceramica pe Zirconiu Total Fizionimica", 900);
					write.WriteLine("Coroana Ceramica pe Implant 1000");
					preturi.Add("Coroana Ceramica pe Implant", 1000);
					write.WriteLine("Proteza Acrilica 1500");
					preturi.Add("Proteza Acrilica", 1500);
					write.WriteLine("Proteza Elastica 1900");
					preturi.Add("Proteza Elastica", 1900);
					write.WriteLine("Dispozitiv Corono-Radicular 250");
					preturi.Add("Dispozitiv Corono-Radicular", 250);
					write.WriteLine("Proteza Provizorie pe Implanturi All-On-4 7500");
					preturi.Add("Proteza Provizorie pe Implanturi All-On-4", 7500);
					write.WriteLine("Proteza Definitiva pe Implanturi All-On-4 12500");
					preturi.Add("Proteza Definitiva pe Implanturi All-On-4", 12500);
					write.WriteLine("Proteza Definitiva pe Implanturi All-On-6 15000");
					preturi.Add("Proteza Definitiva pe Implanturi All-On-6", 15000);
					write.WriteLine("Proteza pe Implanturi Ball-Attachement 5000");
					preturi.Add("Proteza pe Implanturi Ball-Attachement", 5000);
					write.WriteLine("Cimentare 50");
					preturi.Add("Cimentare", 50);
					write.WriteLine("Extractie Dentara 150");
					preturi.Add("Extractie Dentara", 150);
					write.WriteLine("Extractie Molar Minte 200");
					preturi.Add("Extractie Molar Minte", 200);
					write.WriteLine("Tratament Laser pe dinte (chiuretaj gingivectomie etc.) 50");
					preturi.Add("Tratament Laser pe dinte (chiuretaj gingivectomie etc.)", 50);

					write.Close();
				}
			}
		}
	
		public static Dictionary<string, int> GetPrices()
		{
			return preturi;
		}
	}
}

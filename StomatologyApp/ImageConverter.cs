﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StomatologyApp
{
	public static class ImageConverter
	{
		public static Image ByteArrayToImage(byte[] arr)
		{
			MemoryStream ms = new MemoryStream(arr, 0, arr.Length);
			ms.Write(arr, 0, arr.Length);
			var returnImage = Image.FromStream(ms, true);
			return returnImage;
		}

		public static byte[] ImageToByteArray(Image img)
		{
			byte[] byteArray = new byte[0];
			using (MemoryStream stream = new MemoryStream())
			{
				img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
				stream.Close();

				byteArray = stream.ToArray();
			}
			return byteArray;
		}
	}
}

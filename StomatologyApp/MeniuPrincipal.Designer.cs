﻿namespace StomatologyApp
{
	partial class MeniuPrincipal
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MeniuPrincipal));
			this.label1 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.SchimbaCalea = new System.Windows.Forms.Button();
			this.BrowseDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.NouaProgramare = new System.Windows.Forms.Button();
			this.VeziPacient = new System.Windows.Forms.Button();
			this.VeziPacientiAzi = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label1.Font = new System.Drawing.Font("Rockwell", 25F);
			this.label1.Location = new System.Drawing.Point(246, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(264, 40);
			this.label1.TabIndex = 0;
			this.label1.Text = "Meniu Principal";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(246, 94);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(603, 570);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 4;
			this.pictureBox1.TabStop = false;
			// 
			// SchimbaCalea
			// 
			this.SchimbaCalea.Cursor = System.Windows.Forms.Cursors.Hand;
			this.SchimbaCalea.Font = new System.Drawing.Font("Rockwell", 25F);
			this.SchimbaCalea.Location = new System.Drawing.Point(12, 468);
			this.SchimbaCalea.Name = "SchimbaCalea";
			this.SchimbaCalea.Size = new System.Drawing.Size(331, 97);
			this.SchimbaCalea.TabIndex = 5;
			this.SchimbaCalea.Text = "Schimba calea datelor";
			this.SchimbaCalea.UseVisualStyleBackColor = true;
			this.SchimbaCalea.Click += new System.EventHandler(this.SchimbaCalea_Click);
			// 
			// NouaProgramare
			// 
			this.NouaProgramare.Cursor = System.Windows.Forms.Cursors.Hand;
			this.NouaProgramare.Font = new System.Drawing.Font("Rockwell", 25F);
			this.NouaProgramare.Location = new System.Drawing.Point(12, 159);
			this.NouaProgramare.Name = "NouaProgramare";
			this.NouaProgramare.Size = new System.Drawing.Size(331, 97);
			this.NouaProgramare.TabIndex = 6;
			this.NouaProgramare.Text = "Noua Programare";
			this.NouaProgramare.UseVisualStyleBackColor = true;
			this.NouaProgramare.Click += new System.EventHandler(this.NouaRadiografie_Click);
			// 
			// VeziPacient
			// 
			this.VeziPacient.Cursor = System.Windows.Forms.Cursors.Hand;
			this.VeziPacient.Font = new System.Drawing.Font("Rockwell", 25F);
			this.VeziPacient.Location = new System.Drawing.Point(12, 262);
			this.VeziPacient.Name = "VeziPacient";
			this.VeziPacient.Size = new System.Drawing.Size(331, 97);
			this.VeziPacient.TabIndex = 8;
			this.VeziPacient.Text = "Vezi Pacient";
			this.VeziPacient.UseVisualStyleBackColor = true;
			this.VeziPacient.Click += new System.EventHandler(this.VeziPacient_Click);
			// 
			// VeziPacientiAzi
			// 
			this.VeziPacientiAzi.Cursor = System.Windows.Forms.Cursors.Hand;
			this.VeziPacientiAzi.Font = new System.Drawing.Font("Rockwell", 25F);
			this.VeziPacientiAzi.Location = new System.Drawing.Point(12, 365);
			this.VeziPacientiAzi.Name = "VeziPacientiAzi";
			this.VeziPacientiAzi.Size = new System.Drawing.Size(331, 97);
			this.VeziPacientiAzi.TabIndex = 9;
			this.VeziPacientiAzi.Text = "Pacientii programati astazi";
			this.VeziPacientiAzi.UseVisualStyleBackColor = true;
			this.VeziPacientiAzi.Click += new System.EventHandler(this.VeziPacientiAzi_Click);
			// 
			// MeniuPrincipal
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.ClientSize = new System.Drawing.Size(780, 641);
			this.Controls.Add(this.VeziPacientiAzi);
			this.Controls.Add(this.VeziPacient);
			this.Controls.Add(this.NouaProgramare);
			this.Controls.Add(this.SchimbaCalea);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.label1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MeniuPrincipal";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "CMITA App";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button SchimbaCalea;
		private System.Windows.Forms.FolderBrowserDialog BrowseDialog;
		private System.Windows.Forms.Button NouaProgramare;
		private System.Windows.Forms.Button VeziPacient;
		private System.Windows.Forms.Button VeziPacientiAzi;
	}
}


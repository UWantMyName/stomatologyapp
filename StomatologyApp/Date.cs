﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StomatologyApp
{
	public enum Luna
	{
		INVALID,
		Ianuarie,
		Februarie,
		Martie,
		Aprilie,
		Mai,
		Iunie,
		Iulie,
		August,
		Septembrie,
		Octombrie,
		Noiembrie,
		Decembrie
	}

	public class Date
	{
		private int zi;
		private Luna luna;
		private int an;

		/// <summary>
		/// Main constructor for Date.
		/// </summary>
		public Date() 
		{
			zi = 0;
			luna = Luna.INVALID;
			an = 0;
		}

		/// <summary>
		/// Second Constructor for Date.
		/// </summary>
		/// <param name="zi"></param>
		/// <param name="luna"></param>
		/// <param name="an"></param>
		public Date(int zi, int luna, int an)
		{
			this.zi = zi;
			this.luna = GetMonthNumber(luna);
			this.an = an;
		}

		public Date(string zi, string luna, string an)
		{
			this.zi = int.Parse(zi);
			switch(luna)
			{
				case "Ianuarie":
					this.luna = Luna.Ianuarie;
					break;
				case "Februarie":
					this.luna = Luna.Februarie;
					break;
				case "Martie":
					this.luna = Luna.Martie;
					break;
				case "Aprilie":
					this.luna = Luna.Aprilie;
					break;
				case "Mai":
					this.luna = Luna.Mai;
					break;
				case "Iunie":
					this.luna = Luna.Iunie;
					break;
				case "Iulie":
					this.luna = Luna.Iulie;
					break;
				case "August":
					this.luna = Luna.August;
					break;
				case "Septembrie":
					this.luna = Luna.Septembrie;
					break;
				case "Octombrie":
					this.luna = Luna.Octombrie;
					break;
				case "Noiembrie":
					this.luna = Luna.Noiembrie;
					break;
				case "Decembrie":
					this.luna = Luna.Decembrie;
					break;
			}
			this.an = int.Parse(an);
		}

		/// <summary>
		/// Setter for zi.
		/// </summary>
		/// <param name="zi"></param>
		public void SetZi(int zi) { this.zi = zi; }

		/// <summary>
		/// First setter for luna.
		/// </summary>
		/// <param name="luna"></param>
		public void SetLuna(Luna luna) { this.luna = luna; }

		/// <summary>
		/// Second setter for luna.
		/// </summary>
		/// <param name="x"></param>
		public void SetLuna(int x) { this.luna = GetMonthNumber(x); }

		/// <summary>
		/// Setter for an.
		/// </summary>
		/// <param name="an"></param>
		public void SetAn(int an) { this.an = an; }


		/// <summary>
		/// Getter for Zi.
		/// </summary>
		/// <returns></returns>
		public int GetZi() { return this.zi; }

		/// <summary>
		/// Getter for Luna.
		/// </summary>
		/// <returns></returns>
		public Luna GetLuna() { return this.luna; }
		
		/// <summary>
		/// Getter for An.
		/// </summary>
		/// <returns></returns>
		public int GetAn() { return this.an; }


		/// <summary>
		/// Converts the Luna enum into a string representation.
		/// </summary>
		/// <param name="luna"></param>
		/// <returns></returns>
		private string GetNumberMonth(Luna luna)
		{
			if (luna == Luna.Ianuarie) return "01";
			if (luna == Luna.Februarie) return "02";
			if (luna == Luna.Martie) return "03";
			if (luna == Luna.Aprilie) return "04";
			if (luna == Luna.Mai) return "05";
			if (luna == Luna.Iunie) return "06";
			if (luna == Luna.Iulie) return "07";
			if (luna == Luna.August) return "08";
			if (luna == Luna.Septembrie) return "09";
			if (luna == Luna.Octombrie) return "10";
			if (luna == Luna.Noiembrie) return "11";
			if (luna == Luna.Decembrie) return "12";
			return null;
		}

		/// <summary>
		/// Returns the month based on the number given.
		/// </summary>
		/// <param name="x"> The number of the month </param>
		/// <returns> The month that is correctly represented by the number given. </returns>
		private Luna GetMonthNumber(int x)
		{
			if (x == 1) return Luna.Ianuarie;
			if (x == 2) return Luna.Februarie;
			if (x == 3) return Luna.Martie;
			if (x == 4) return Luna.Aprilie;
			if (x == 5) return Luna.Mai;
			if (x == 6) return Luna.Iunie;
			if (x == 7) return Luna.Iulie;
			if (x == 8) return Luna.August;
			if (x == 9) return Luna.Septembrie;
			if (x == 10) return Luna.Octombrie;
			if (x == 11) return Luna.Noiembrie;
			if (x == 12) return Luna.Decembrie;
			return Luna.INVALID;
		}

		/// <summary>
		/// Returns a string representation of the Date
		/// </summary>
		/// <returns> Returns the date as a string with numbers representing the day/month/year. </returns>
		public override string ToString()
		{

			return this.zi + " " + this.luna.ToString() + " " + this.an;
		}
	}
}

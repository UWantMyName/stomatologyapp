﻿
namespace StomatologyApp
{
	partial class DetaliiPacientSelectat
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DetaliiPacientSelectat));
			this.Editeaza_Button = new System.Windows.Forms.Button();
			this.Salveaza_Button = new System.Windows.Forms.Button();
			this.DetailsPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.RadiografieNoua = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.Adresa_Domiciliu = new System.Windows.Forms.GroupBox();
			this.Judetul_TextBox = new System.Windows.Forms.TextBox();
			this.Localitatea_TextBox = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.Adresa_TextBox = new System.Windows.Forms.TextBox();
			this.Detalii_Personale = new System.Windows.Forms.GroupBox();
			this.Sex_TextBox = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.Email_TextBox = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.NrTelefon_TextBox = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.CNP_TextBox = new System.Windows.Forms.TextBox();
			this.Prenume_TextBox = new System.Windows.Forms.TextBox();
			this.Nume_TextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.Programari_GB = new System.Windows.Forms.GroupBox();
			this.buttonsPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.AddNew_Button = new System.Windows.Forms.Button();
			this.Adresa_Domiciliu.SuspendLayout();
			this.Detalii_Personale.SuspendLayout();
			this.Programari_GB.SuspendLayout();
			this.SuspendLayout();
			// 
			// Editeaza_Button
			// 
			this.Editeaza_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Editeaza_Button.Font = new System.Drawing.Font("Rockwell", 25F);
			this.Editeaza_Button.Location = new System.Drawing.Point(545, 690);
			this.Editeaza_Button.Name = "Editeaza_Button";
			this.Editeaza_Button.Size = new System.Drawing.Size(169, 66);
			this.Editeaza_Button.TabIndex = 26;
			this.Editeaza_Button.Text = "Editeaza";
			this.Editeaza_Button.UseVisualStyleBackColor = true;
			this.Editeaza_Button.Visible = false;
			this.Editeaza_Button.Click += new System.EventHandler(this.Editeaza_Button_Click);
			// 
			// Salveaza_Button
			// 
			this.Salveaza_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Salveaza_Button.Font = new System.Drawing.Font("Rockwell", 25F);
			this.Salveaza_Button.Location = new System.Drawing.Point(370, 690);
			this.Salveaza_Button.Name = "Salveaza_Button";
			this.Salveaza_Button.Size = new System.Drawing.Size(169, 66);
			this.Salveaza_Button.TabIndex = 27;
			this.Salveaza_Button.Text = "Salveaza";
			this.Salveaza_Button.UseVisualStyleBackColor = true;
			this.Salveaza_Button.Visible = false;
			this.Salveaza_Button.Click += new System.EventHandler(this.Salveaza_Button_Click);
			// 
			// DetailsPanel
			// 
			this.DetailsPanel.AutoScroll = true;
			this.DetailsPanel.Location = new System.Drawing.Point(729, 690);
			this.DetailsPanel.Name = "DetailsPanel";
			this.DetailsPanel.Size = new System.Drawing.Size(352, 511);
			this.DetailsPanel.TabIndex = 28;
			// 
			// RadiografieNoua
			// 
			this.RadiografieNoua.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.RadiografieNoua.Font = new System.Drawing.Font("Rockwell", 25F);
			this.RadiografieNoua.Location = new System.Drawing.Point(12, 690);
			this.RadiografieNoua.Name = "RadiografieNoua";
			this.RadiografieNoua.Size = new System.Drawing.Size(352, 66);
			this.RadiografieNoua.TabIndex = 29;
			this.RadiografieNoua.Text = "Radiografie Noua";
			this.RadiografieNoua.UseVisualStyleBackColor = true;
			this.RadiografieNoua.Visible = false;
			this.RadiografieNoua.Click += new System.EventHandler(this.RadiografieNoua_Click);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			// 
			// Adresa_Domiciliu
			// 
			this.Adresa_Domiciliu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Adresa_Domiciliu.Controls.Add(this.Judetul_TextBox);
			this.Adresa_Domiciliu.Controls.Add(this.Localitatea_TextBox);
			this.Adresa_Domiciliu.Controls.Add(this.label12);
			this.Adresa_Domiciliu.Controls.Add(this.label11);
			this.Adresa_Domiciliu.Controls.Add(this.Adresa_TextBox);
			this.Adresa_Domiciliu.Font = new System.Drawing.Font("Rockwell", 15F);
			this.Adresa_Domiciliu.ForeColor = System.Drawing.Color.DarkGreen;
			this.Adresa_Domiciliu.Location = new System.Drawing.Point(12, 292);
			this.Adresa_Domiciliu.Name = "Adresa_Domiciliu";
			this.Adresa_Domiciliu.Size = new System.Drawing.Size(424, 275);
			this.Adresa_Domiciliu.TabIndex = 31;
			this.Adresa_Domiciliu.TabStop = false;
			this.Adresa_Domiciliu.Text = "Adresa Domiciliu";
			// 
			// Judetul_TextBox
			// 
			this.Judetul_TextBox.Location = new System.Drawing.Point(95, 222);
			this.Judetul_TextBox.Name = "Judetul_TextBox";
			this.Judetul_TextBox.ReadOnly = true;
			this.Judetul_TextBox.Size = new System.Drawing.Size(314, 31);
			this.Judetul_TextBox.TabIndex = 10;
			// 
			// Localitatea_TextBox
			// 
			this.Localitatea_TextBox.Location = new System.Drawing.Point(126, 180);
			this.Localitatea_TextBox.Name = "Localitatea_TextBox";
			this.Localitatea_TextBox.ReadOnly = true;
			this.Localitatea_TextBox.Size = new System.Drawing.Size(283, 31);
			this.Localitatea_TextBox.TabIndex = 9;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label12.Location = new System.Drawing.Point(7, 223);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(82, 36);
			this.label12.TabIndex = 107;
			this.label12.Text = "Judetul:";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label11.Location = new System.Drawing.Point(7, 180);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(113, 36);
			this.label11.TabIndex = 106;
			this.label11.Text = "Localitatea:";
			// 
			// Adresa_TextBox
			// 
			this.Adresa_TextBox.Location = new System.Drawing.Point(13, 30);
			this.Adresa_TextBox.Multiline = true;
			this.Adresa_TextBox.Name = "Adresa_TextBox";
			this.Adresa_TextBox.ReadOnly = true;
			this.Adresa_TextBox.Size = new System.Drawing.Size(396, 132);
			this.Adresa_TextBox.TabIndex = 8;
			// 
			// Detalii_Personale
			// 
			this.Detalii_Personale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Detalii_Personale.Controls.Add(this.Sex_TextBox);
			this.Detalii_Personale.Controls.Add(this.label14);
			this.Detalii_Personale.Controls.Add(this.Email_TextBox);
			this.Detalii_Personale.Controls.Add(this.label13);
			this.Detalii_Personale.Controls.Add(this.NrTelefon_TextBox);
			this.Detalii_Personale.Controls.Add(this.label8);
			this.Detalii_Personale.Controls.Add(this.CNP_TextBox);
			this.Detalii_Personale.Controls.Add(this.Prenume_TextBox);
			this.Detalii_Personale.Controls.Add(this.Nume_TextBox);
			this.Detalii_Personale.Controls.Add(this.label4);
			this.Detalii_Personale.Controls.Add(this.label3);
			this.Detalii_Personale.Controls.Add(this.label2);
			this.Detalii_Personale.Font = new System.Drawing.Font("Rockwell", 15F);
			this.Detalii_Personale.ForeColor = System.Drawing.Color.DarkGreen;
			this.Detalii_Personale.Location = new System.Drawing.Point(12, 12);
			this.Detalii_Personale.Name = "Detalii_Personale";
			this.Detalii_Personale.Size = new System.Drawing.Size(424, 274);
			this.Detalii_Personale.TabIndex = 30;
			this.Detalii_Personale.TabStop = false;
			this.Detalii_Personale.Text = "Detalii Pacient";
			// 
			// Sex_TextBox
			// 
			this.Sex_TextBox.Location = new System.Drawing.Point(78, 223);
			this.Sex_TextBox.Name = "Sex_TextBox";
			this.Sex_TextBox.ReadOnly = true;
			this.Sex_TextBox.Size = new System.Drawing.Size(331, 31);
			this.Sex_TextBox.TabIndex = 6;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label14.Location = new System.Drawing.Point(6, 223);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(54, 36);
			this.label14.TabIndex = 105;
			this.label14.Text = "Sex: ";
			// 
			// Email_TextBox
			// 
			this.Email_TextBox.Location = new System.Drawing.Point(78, 187);
			this.Email_TextBox.Name = "Email_TextBox";
			this.Email_TextBox.ReadOnly = true;
			this.Email_TextBox.Size = new System.Drawing.Size(331, 31);
			this.Email_TextBox.TabIndex = 5;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label13.Location = new System.Drawing.Point(6, 187);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(66, 36);
			this.label13.TabIndex = 104;
			this.label13.Text = "Email:";
			// 
			// NrTelefon_TextBox
			// 
			this.NrTelefon_TextBox.Location = new System.Drawing.Point(119, 150);
			this.NrTelefon_TextBox.Name = "NrTelefon_TextBox";
			this.NrTelefon_TextBox.ReadOnly = true;
			this.NrTelefon_TextBox.Size = new System.Drawing.Size(290, 31);
			this.NrTelefon_TextBox.TabIndex = 4;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label8.Location = new System.Drawing.Point(6, 150);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(107, 36);
			this.label8.TabIndex = 103;
			this.label8.Text = "Nr telefon:";
			// 
			// CNP_TextBox
			// 
			this.CNP_TextBox.Location = new System.Drawing.Point(69, 113);
			this.CNP_TextBox.Name = "CNP_TextBox";
			this.CNP_TextBox.ReadOnly = true;
			this.CNP_TextBox.Size = new System.Drawing.Size(340, 31);
			this.CNP_TextBox.TabIndex = 3;
			// 
			// Prenume_TextBox
			// 
			this.Prenume_TextBox.Location = new System.Drawing.Point(113, 75);
			this.Prenume_TextBox.Name = "Prenume_TextBox";
			this.Prenume_TextBox.ReadOnly = true;
			this.Prenume_TextBox.Size = new System.Drawing.Size(296, 31);
			this.Prenume_TextBox.TabIndex = 2;
			// 
			// Nume_TextBox
			// 
			this.Nume_TextBox.Location = new System.Drawing.Point(99, 38);
			this.Nume_TextBox.Name = "Nume_TextBox";
			this.Nume_TextBox.ReadOnly = true;
			this.Nume_TextBox.Size = new System.Drawing.Size(310, 31);
			this.Nume_TextBox.TabIndex = 1;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label4.Location = new System.Drawing.Point(6, 113);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(57, 36);
			this.label4.TabIndex = 102;
			this.label4.Text = "CNP:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label3.Location = new System.Drawing.Point(6, 75);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(101, 36);
			this.label3.TabIndex = 101;
			this.label3.Text = "Prenume: ";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label2.Location = new System.Drawing.Point(6, 39);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 36);
			this.label2.TabIndex = 100;
			this.label2.Text = "Nume:";
			// 
			// Programari_GB
			// 
			this.Programari_GB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Programari_GB.Controls.Add(this.buttonsPanel);
			this.Programari_GB.Controls.Add(this.AddNew_Button);
			this.Programari_GB.Font = new System.Drawing.Font("Rockwell", 15F);
			this.Programari_GB.ForeColor = System.Drawing.Color.DarkGreen;
			this.Programari_GB.Location = new System.Drawing.Point(455, 18);
			this.Programari_GB.Name = "Programari_GB";
			this.Programari_GB.Size = new System.Drawing.Size(401, 549);
			this.Programari_GB.TabIndex = 19;
			this.Programari_GB.TabStop = false;
			this.Programari_GB.Text = "Programari";
			// 
			// buttonsPanel
			// 
			this.buttonsPanel.AutoScroll = true;
			this.buttonsPanel.Location = new System.Drawing.Point(7, 31);
			this.buttonsPanel.Name = "buttonsPanel";
			this.buttonsPanel.Size = new System.Drawing.Size(385, 465);
			this.buttonsPanel.TabIndex = 0;
			// 
			// AddNew_Button
			// 
			this.AddNew_Button.BackColor = System.Drawing.Color.White;
			this.AddNew_Button.Cursor = System.Windows.Forms.Cursors.Hand;
			this.AddNew_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.AddNew_Button.Font = new System.Drawing.Font("Rockwell", 12F);
			this.AddNew_Button.ForeColor = System.Drawing.Color.Black;
			this.AddNew_Button.Location = new System.Drawing.Point(7, 502);
			this.AddNew_Button.Name = "AddNew_Button";
			this.AddNew_Button.Size = new System.Drawing.Size(385, 32);
			this.AddNew_Button.TabIndex = 19;
			this.AddNew_Button.Text = "Programare Noua";
			this.AddNew_Button.UseVisualStyleBackColor = false;
			this.AddNew_Button.Click += new System.EventHandler(this.AddNew_Button_Click);
			// 
			// DetaliiPacientSelectat
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(865, 582);
			this.Controls.Add(this.Programari_GB);
			this.Controls.Add(this.Adresa_Domiciliu);
			this.Controls.Add(this.Detalii_Personale);
			this.Controls.Add(this.RadiografieNoua);
			this.Controls.Add(this.DetailsPanel);
			this.Controls.Add(this.Salveaza_Button);
			this.Controls.Add(this.Editeaza_Button);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "DetaliiPacientSelectat";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Detalii";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DetaliiPacientSelectat_FormClosing);
			this.Adresa_Domiciliu.ResumeLayout(false);
			this.Adresa_Domiciliu.PerformLayout();
			this.Detalii_Personale.ResumeLayout(false);
			this.Detalii_Personale.PerformLayout();
			this.Programari_GB.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Button Editeaza_Button;
		private System.Windows.Forms.Button Salveaza_Button;
		private System.Windows.Forms.FlowLayoutPanel DetailsPanel;
		private System.Windows.Forms.Button RadiografieNoua;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.GroupBox Adresa_Domiciliu;
		private System.Windows.Forms.TextBox Judetul_TextBox;
		private System.Windows.Forms.TextBox Localitatea_TextBox;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox Adresa_TextBox;
		private System.Windows.Forms.GroupBox Detalii_Personale;
		private System.Windows.Forms.TextBox Sex_TextBox;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox Email_TextBox;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox NrTelefon_TextBox;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox CNP_TextBox;
		private System.Windows.Forms.TextBox Prenume_TextBox;
		private System.Windows.Forms.TextBox Nume_TextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox Programari_GB;
		private System.Windows.Forms.FlowLayoutPanel buttonsPanel;
		private System.Windows.Forms.Button AddNew_Button;
	}
}
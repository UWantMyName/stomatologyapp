﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StomatologyApp
{
	public class ListaRadiografii
	{   
		private List<Radiografie> radiografii = new List<Radiografie>();

		public void SetRadiografii(List<Radiografie> rad)
		{
			this.radiografii = rad;
		}

		public List<Radiografie> GetRadiografii() { return this.radiografii; }
	}
}

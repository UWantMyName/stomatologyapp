﻿namespace StomatologyApp
{
	partial class VeziDB
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VeziDB));
			this.PacientiView = new System.Windows.Forms.DataGridView();
			this.Afiseaza = new System.Windows.Forms.Button();
			this.SchimbaAfisare = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.PacientiView)).BeginInit();
			this.SuspendLayout();
			// 
			// PacientiView
			// 
			this.PacientiView.AllowUserToAddRows = false;
			this.PacientiView.AllowUserToDeleteRows = false;
			this.PacientiView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.PacientiView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
			this.PacientiView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.PacientiView.Location = new System.Drawing.Point(12, 83);
			this.PacientiView.Name = "PacientiView";
			this.PacientiView.ReadOnly = true;
			this.PacientiView.Size = new System.Drawing.Size(1147, 501);
			this.PacientiView.TabIndex = 0;
			// 
			// Afiseaza
			// 
			this.Afiseaza.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Afiseaza.Font = new System.Drawing.Font("Rockwell", 25F);
			this.Afiseaza.Location = new System.Drawing.Point(490, 599);
			this.Afiseaza.Name = "Afiseaza";
			this.Afiseaza.Size = new System.Drawing.Size(187, 70);
			this.Afiseaza.TabIndex = 2;
			this.Afiseaza.Text = "Afiseaza";
			this.Afiseaza.UseVisualStyleBackColor = true;
			this.Afiseaza.Click += new System.EventHandler(this.Afiseaza_Click);
			// 
			// SchimbaAfisare
			// 
			this.SchimbaAfisare.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.SchimbaAfisare.Font = new System.Drawing.Font("Rockwell", 20F);
			this.SchimbaAfisare.Location = new System.Drawing.Point(919, 12);
			this.SchimbaAfisare.Name = "SchimbaAfisare";
			this.SchimbaAfisare.Size = new System.Drawing.Size(240, 52);
			this.SchimbaAfisare.TabIndex = 3;
			this.SchimbaAfisare.Text = "Alege Atribute";
			this.SchimbaAfisare.UseVisualStyleBackColor = true;
			this.SchimbaAfisare.Click += new System.EventHandler(this.SchimbaAfisare_Click);
			// 
			// VeziDB
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1171, 681);
			this.Controls.Add(this.SchimbaAfisare);
			this.Controls.Add(this.Afiseaza);
			this.Controls.Add(this.PacientiView);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "VeziDB";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "VeziDB";
			((System.ComponentModel.ISupportInitialize)(this.PacientiView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView PacientiView;
		private System.Windows.Forms.Button Afiseaza;
		private System.Windows.Forms.Button SchimbaAfisare;
	}
}
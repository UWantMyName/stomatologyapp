﻿namespace StomatologyApp
{
	partial class NouaProgramare
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NouaProgramare));
			this.label1 = new System.Windows.Forms.Label();
			this.Detalii_Personale = new System.Windows.Forms.GroupBox();
			this.Sex_TextBox = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.Email_TextBox = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.NrTelefon_TextBox = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.CNP_TextBox = new System.Windows.Forms.TextBox();
			this.Prenume_TextBox = new System.Windows.Forms.TextBox();
			this.Nume_TextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.Adresa_Domiciliu = new System.Windows.Forms.GroupBox();
			this.Judetul_TextBox = new System.Windows.Forms.TextBox();
			this.Localitatea_TextBox = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.Adresa_TextBox = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.DataProgramare = new System.Windows.Forms.DateTimePicker();
			this.Remediu_GB = new System.Windows.Forms.GroupBox();
			this.StergeTratament_Button = new System.Windows.Forms.Button();
			this.Toti_Button = new System.Windows.Forms.Button();
			this.Sterge_DintiAfectati = new System.Windows.Forms.Button();
			this.DintiAfectati_ComboBox = new System.Windows.Forms.ComboBox();
			this.Dinti_Panel = new System.Windows.Forms.FlowLayoutPanel();
			this.label6 = new System.Windows.Forms.Label();
			this.Labels_Panel = new System.Windows.Forms.FlowLayoutPanel();
			this.Prices_CB = new System.Windows.Forms.ComboBox();
			this.Min_CB = new System.Windows.Forms.ComboBox();
			this.label16 = new System.Windows.Forms.Label();
			this.Hour_CB = new System.Windows.Forms.ComboBox();
			this.label15 = new System.Windows.Forms.Label();
			this.Total_Label = new System.Windows.Forms.Label();
			this.Diagnostic_TB = new System.Windows.Forms.RichTextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.Salveaza_Button = new System.Windows.Forms.Button();
			this.Radiografii_GB = new System.Windows.Forms.GroupBox();
			this.buttonsPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.AddNew_Button = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.Progress = new System.Windows.Forms.ProgressBar();
			this.Detalii_Personale.SuspendLayout();
			this.Adresa_Domiciliu.SuspendLayout();
			this.Remediu_GB.SuspendLayout();
			this.Radiografii_GB.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label1.Font = new System.Drawing.Font("Rockwell", 25F);
			this.label1.Location = new System.Drawing.Point(533, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(404, 40);
			this.label1.TabIndex = 1;
			this.label1.Text = "Detalii Programare Noua";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Detalii_Personale
			// 
			this.Detalii_Personale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Detalii_Personale.Controls.Add(this.Sex_TextBox);
			this.Detalii_Personale.Controls.Add(this.label14);
			this.Detalii_Personale.Controls.Add(this.Email_TextBox);
			this.Detalii_Personale.Controls.Add(this.label13);
			this.Detalii_Personale.Controls.Add(this.NrTelefon_TextBox);
			this.Detalii_Personale.Controls.Add(this.label8);
			this.Detalii_Personale.Controls.Add(this.CNP_TextBox);
			this.Detalii_Personale.Controls.Add(this.Prenume_TextBox);
			this.Detalii_Personale.Controls.Add(this.Nume_TextBox);
			this.Detalii_Personale.Controls.Add(this.label4);
			this.Detalii_Personale.Controls.Add(this.label3);
			this.Detalii_Personale.Controls.Add(this.label2);
			this.Detalii_Personale.Font = new System.Drawing.Font("Rockwell", 15F);
			this.Detalii_Personale.ForeColor = System.Drawing.Color.DarkGreen;
			this.Detalii_Personale.Location = new System.Drawing.Point(12, 64);
			this.Detalii_Personale.Name = "Detalii_Personale";
			this.Detalii_Personale.Size = new System.Drawing.Size(352, 274);
			this.Detalii_Personale.TabIndex = 0;
			this.Detalii_Personale.TabStop = false;
			this.Detalii_Personale.Text = "Detalii Pacient";
			// 
			// Sex_TextBox
			// 
			this.Sex_TextBox.Location = new System.Drawing.Point(78, 223);
			this.Sex_TextBox.Name = "Sex_TextBox";
			this.Sex_TextBox.Size = new System.Drawing.Size(244, 31);
			this.Sex_TextBox.TabIndex = 6;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label14.Location = new System.Drawing.Point(6, 223);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(62, 36);
			this.label14.TabIndex = 105;
			this.label14.Text = "Sex*: ";
			// 
			// Email_TextBox
			// 
			this.Email_TextBox.Location = new System.Drawing.Point(78, 187);
			this.Email_TextBox.Name = "Email_TextBox";
			this.Email_TextBox.Size = new System.Drawing.Size(244, 31);
			this.Email_TextBox.TabIndex = 5;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label13.Location = new System.Drawing.Point(6, 187);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(66, 36);
			this.label13.TabIndex = 104;
			this.label13.Text = "Email:";
			// 
			// NrTelefon_TextBox
			// 
			this.NrTelefon_TextBox.Location = new System.Drawing.Point(119, 150);
			this.NrTelefon_TextBox.Name = "NrTelefon_TextBox";
			this.NrTelefon_TextBox.Size = new System.Drawing.Size(203, 31);
			this.NrTelefon_TextBox.TabIndex = 4;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label8.Location = new System.Drawing.Point(6, 150);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(115, 36);
			this.label8.TabIndex = 103;
			this.label8.Text = "Nr telefon*:";
			// 
			// CNP_TextBox
			// 
			this.CNP_TextBox.Location = new System.Drawing.Point(69, 113);
			this.CNP_TextBox.Name = "CNP_TextBox";
			this.CNP_TextBox.Size = new System.Drawing.Size(253, 31);
			this.CNP_TextBox.TabIndex = 3;
			// 
			// Prenume_TextBox
			// 
			this.Prenume_TextBox.Location = new System.Drawing.Point(113, 75);
			this.Prenume_TextBox.Name = "Prenume_TextBox";
			this.Prenume_TextBox.Size = new System.Drawing.Size(209, 31);
			this.Prenume_TextBox.TabIndex = 2;
			// 
			// Nume_TextBox
			// 
			this.Nume_TextBox.Location = new System.Drawing.Point(99, 38);
			this.Nume_TextBox.Name = "Nume_TextBox";
			this.Nume_TextBox.Size = new System.Drawing.Size(223, 31);
			this.Nume_TextBox.TabIndex = 1;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label4.Location = new System.Drawing.Point(6, 113);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(65, 36);
			this.label4.TabIndex = 102;
			this.label4.Text = "CNP*:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label3.Location = new System.Drawing.Point(6, 75);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(109, 36);
			this.label3.TabIndex = 101;
			this.label3.Text = "Prenume*: ";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label2.Location = new System.Drawing.Point(6, 39);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(80, 36);
			this.label2.TabIndex = 100;
			this.label2.Text = "Nume*:";
			// 
			// Adresa_Domiciliu
			// 
			this.Adresa_Domiciliu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Adresa_Domiciliu.Controls.Add(this.Judetul_TextBox);
			this.Adresa_Domiciliu.Controls.Add(this.Localitatea_TextBox);
			this.Adresa_Domiciliu.Controls.Add(this.label12);
			this.Adresa_Domiciliu.Controls.Add(this.label11);
			this.Adresa_Domiciliu.Controls.Add(this.Adresa_TextBox);
			this.Adresa_Domiciliu.Font = new System.Drawing.Font("Rockwell", 15F);
			this.Adresa_Domiciliu.ForeColor = System.Drawing.Color.DarkGreen;
			this.Adresa_Domiciliu.Location = new System.Drawing.Point(12, 344);
			this.Adresa_Domiciliu.Name = "Adresa_Domiciliu";
			this.Adresa_Domiciliu.Size = new System.Drawing.Size(352, 269);
			this.Adresa_Domiciliu.TabIndex = 7;
			this.Adresa_Domiciliu.TabStop = false;
			this.Adresa_Domiciliu.Text = "Adresa Domiciliu";
			// 
			// Judetul_TextBox
			// 
			this.Judetul_TextBox.Location = new System.Drawing.Point(95, 222);
			this.Judetul_TextBox.Name = "Judetul_TextBox";
			this.Judetul_TextBox.Size = new System.Drawing.Size(241, 31);
			this.Judetul_TextBox.TabIndex = 10;
			// 
			// Localitatea_TextBox
			// 
			this.Localitatea_TextBox.Location = new System.Drawing.Point(126, 180);
			this.Localitatea_TextBox.Name = "Localitatea_TextBox";
			this.Localitatea_TextBox.Size = new System.Drawing.Size(210, 31);
			this.Localitatea_TextBox.TabIndex = 9;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label12.Location = new System.Drawing.Point(7, 223);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(90, 36);
			this.label12.TabIndex = 107;
			this.label12.Text = "Judetul*:";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label11.Location = new System.Drawing.Point(7, 180);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(121, 36);
			this.label11.TabIndex = 106;
			this.label11.Text = "Localitatea*:";
			// 
			// Adresa_TextBox
			// 
			this.Adresa_TextBox.Location = new System.Drawing.Point(13, 30);
			this.Adresa_TextBox.Multiline = true;
			this.Adresa_TextBox.Name = "Adresa_TextBox";
			this.Adresa_TextBox.Size = new System.Drawing.Size(323, 132);
			this.Adresa_TextBox.TabIndex = 8;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label5.Location = new System.Drawing.Point(14, 39);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(90, 36);
			this.label5.TabIndex = 108;
			this.label5.Text = "La data*:";
			// 
			// DataProgramare
			// 
			this.DataProgramare.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.DataProgramare.Location = new System.Drawing.Point(110, 35);
			this.DataProgramare.Name = "DataProgramare";
			this.DataProgramare.Size = new System.Drawing.Size(148, 31);
			this.DataProgramare.TabIndex = 12;
			this.DataProgramare.Value = new System.DateTime(2021, 1, 1, 0, 0, 0, 0);
			this.DataProgramare.ValueChanged += new System.EventHandler(this.DataProgramare_ValueChanged);
			// 
			// Remediu_GB
			// 
			this.Remediu_GB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Remediu_GB.Controls.Add(this.StergeTratament_Button);
			this.Remediu_GB.Controls.Add(this.Toti_Button);
			this.Remediu_GB.Controls.Add(this.Sterge_DintiAfectati);
			this.Remediu_GB.Controls.Add(this.DintiAfectati_ComboBox);
			this.Remediu_GB.Controls.Add(this.Dinti_Panel);
			this.Remediu_GB.Controls.Add(this.label6);
			this.Remediu_GB.Controls.Add(this.Labels_Panel);
			this.Remediu_GB.Controls.Add(this.Prices_CB);
			this.Remediu_GB.Controls.Add(this.Min_CB);
			this.Remediu_GB.Controls.Add(this.label16);
			this.Remediu_GB.Controls.Add(this.Hour_CB);
			this.Remediu_GB.Controls.Add(this.label15);
			this.Remediu_GB.Controls.Add(this.label5);
			this.Remediu_GB.Controls.Add(this.DataProgramare);
			this.Remediu_GB.Controls.Add(this.Total_Label);
			this.Remediu_GB.Controls.Add(this.Diagnostic_TB);
			this.Remediu_GB.Controls.Add(this.label9);
			this.Remediu_GB.Controls.Add(this.label10);
			this.Remediu_GB.Font = new System.Drawing.Font("Rockwell", 15F);
			this.Remediu_GB.ForeColor = System.Drawing.Color.DarkGreen;
			this.Remediu_GB.Location = new System.Drawing.Point(376, 64);
			this.Remediu_GB.Name = "Remediu_GB";
			this.Remediu_GB.Size = new System.Drawing.Size(730, 549);
			this.Remediu_GB.TabIndex = 11;
			this.Remediu_GB.TabStop = false;
			this.Remediu_GB.Text = "Programat";
			// 
			// StergeTratament_Button
			// 
			this.StergeTratament_Button.BackColor = System.Drawing.Color.WhiteSmoke;
			this.StergeTratament_Button.Cursor = System.Windows.Forms.Cursors.Hand;
			this.StergeTratament_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.StergeTratament_Button.Font = new System.Drawing.Font("Rockwell", 16F);
			this.StergeTratament_Button.ForeColor = System.Drawing.Color.Red;
			this.StergeTratament_Button.Location = new System.Drawing.Point(401, 502);
			this.StergeTratament_Button.Name = "StergeTratament_Button";
			this.StergeTratament_Button.Size = new System.Drawing.Size(288, 34);
			this.StergeTratament_Button.TabIndex = 123;
			this.StergeTratament_Button.Text = "Sterge ultimul tratament";
			this.StergeTratament_Button.UseVisualStyleBackColor = false;
			this.StergeTratament_Button.Click += new System.EventHandler(this.StergeTratament_Button_Click);
			// 
			// Toti_Button
			// 
			this.Toti_Button.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Toti_Button.Cursor = System.Windows.Forms.Cursors.Hand;
			this.Toti_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Toti_Button.Font = new System.Drawing.Font("Rockwell", 16F);
			this.Toti_Button.ForeColor = System.Drawing.Color.Green;
			this.Toti_Button.Location = new System.Drawing.Point(664, 149);
			this.Toti_Button.Name = "Toti_Button";
			this.Toti_Button.Size = new System.Drawing.Size(60, 45);
			this.Toti_Button.TabIndex = 122;
			this.Toti_Button.Text = "Toti";
			this.Toti_Button.UseVisualStyleBackColor = false;
			this.Toti_Button.Click += new System.EventHandler(this.Toti_Button_Click);
			// 
			// Sterge_DintiAfectati
			// 
			this.Sterge_DintiAfectati.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Sterge_DintiAfectati.Cursor = System.Windows.Forms.Cursors.Hand;
			this.Sterge_DintiAfectati.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Sterge_DintiAfectati.Font = new System.Drawing.Font("Rockwell", 20F);
			this.Sterge_DintiAfectati.ForeColor = System.Drawing.Color.Red;
			this.Sterge_DintiAfectati.Location = new System.Drawing.Point(598, 150);
			this.Sterge_DintiAfectati.Name = "Sterge_DintiAfectati";
			this.Sterge_DintiAfectati.Size = new System.Drawing.Size(60, 45);
			this.Sterge_DintiAfectati.TabIndex = 115;
			this.Sterge_DintiAfectati.Text = "X";
			this.Sterge_DintiAfectati.UseVisualStyleBackColor = false;
			this.Sterge_DintiAfectati.Click += new System.EventHandler(this.Sterge_DintiAfectati_Click);
			// 
			// DintiAfectati_ComboBox
			// 
			this.DintiAfectati_ComboBox.DropDownHeight = 200;
			this.DintiAfectati_ComboBox.FormattingEnabled = true;
			this.DintiAfectati_ComboBox.IntegralHeight = false;
			this.DintiAfectati_ComboBox.Items.AddRange(new object[] {
            "1.1",
            "1.2",
            "1.3",
            "1.4",
            "1.5",
            "1.6",
            "1.7",
            "1.8",
            "2.1",
            "2.2",
            "2.3",
            "2.4",
            "2.5",
            "2.6",
            "2.7",
            "2.8",
            "3.1",
            "3.2",
            "3.3",
            "3.4",
            "3.5",
            "3.6",
            "3.7",
            "3.8",
            "4.1",
            "4.2",
            "4.3",
            "4.4",
            "4.5",
            "4.6",
            "4.7",
            "4.8"});
			this.DintiAfectati_ComboBox.Location = new System.Drawing.Point(598, 113);
			this.DintiAfectati_ComboBox.Name = "DintiAfectati_ComboBox";
			this.DintiAfectati_ComboBox.Size = new System.Drawing.Size(126, 30);
			this.DintiAfectati_ComboBox.TabIndex = 16;
			this.DintiAfectati_ComboBox.Text = "Dinti...";
			this.DintiAfectati_ComboBox.SelectedValueChanged += new System.EventHandler(this.DintiAfectati_ComboBox_SelectedValueChanged);
			// 
			// Dinti_Panel
			// 
			this.Dinti_Panel.AutoScroll = true;
			this.Dinti_Panel.BackColor = System.Drawing.SystemColors.Control;
			this.Dinti_Panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Dinti_Panel.Location = new System.Drawing.Point(376, 113);
			this.Dinti_Panel.Name = "Dinti_Panel";
			this.Dinti_Panel.Size = new System.Drawing.Size(216, 85);
			this.Dinti_Panel.TabIndex = 120;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label6.Location = new System.Drawing.Point(370, 78);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(138, 36);
			this.label6.TabIndex = 121;
			this.label6.Text = "Dinti afectati*:";
			// 
			// Labels_Panel
			// 
			this.Labels_Panel.AutoScroll = true;
			this.Labels_Panel.BackColor = System.Drawing.SystemColors.Control;
			this.Labels_Panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Labels_Panel.Location = new System.Drawing.Point(25, 280);
			this.Labels_Panel.Name = "Labels_Panel";
			this.Labels_Panel.Size = new System.Drawing.Size(699, 213);
			this.Labels_Panel.TabIndex = 119;
			// 
			// Prices_CB
			// 
			this.Prices_CB.DropDownHeight = 200;
			this.Prices_CB.FormattingEnabled = true;
			this.Prices_CB.IntegralHeight = false;
			this.Prices_CB.Location = new System.Drawing.Point(25, 243);
			this.Prices_CB.Name = "Prices_CB";
			this.Prices_CB.Size = new System.Drawing.Size(699, 30);
			this.Prices_CB.TabIndex = 17;
			this.Prices_CB.Text = "Alege tratamente..";
			this.Prices_CB.SelectedValueChanged += new System.EventHandler(this.Prices_CB_SelectedValueChanged);
			// 
			// Min_CB
			// 
			this.Min_CB.DropDownHeight = 200;
			this.Min_CB.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Min_CB.FormattingEnabled = true;
			this.Min_CB.IntegralHeight = false;
			this.Min_CB.ItemHeight = 22;
			this.Min_CB.Location = new System.Drawing.Point(523, 32);
			this.Min_CB.Name = "Min_CB";
			this.Min_CB.Size = new System.Drawing.Size(48, 30);
			this.Min_CB.TabIndex = 14;
			this.Min_CB.SelectedIndexChanged += new System.EventHandler(this.Min_CB_SelectedIndexChanged);
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Bold);
			this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label16.Location = new System.Drawing.Point(501, 31);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(20, 36);
			this.label16.TabIndex = 115;
			this.label16.Text = ":";
			// 
			// Hour_CB
			// 
			this.Hour_CB.DropDownHeight = 200;
			this.Hour_CB.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Hour_CB.FormattingEnabled = true;
			this.Hour_CB.IntegralHeight = false;
			this.Hour_CB.ItemHeight = 22;
			this.Hour_CB.Location = new System.Drawing.Point(447, 31);
			this.Hour_CB.Name = "Hour_CB";
			this.Hour_CB.Size = new System.Drawing.Size(48, 30);
			this.Hour_CB.TabIndex = 13;
			this.Hour_CB.SelectedIndexChanged += new System.EventHandler(this.Hour_CB_SelectedIndexChanged);
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label15.Location = new System.Drawing.Point(369, 31);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(80, 36);
			this.label15.TabIndex = 113;
			this.label15.Text = "La ora*:";
			// 
			// Total_Label
			// 
			this.Total_Label.AutoSize = true;
			this.Total_Label.Font = new System.Drawing.Font("Myanmar Text", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Total_Label.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.Total_Label.Location = new System.Drawing.Point(26, 496);
			this.Total_Label.Name = "Total_Label";
			this.Total_Label.Size = new System.Drawing.Size(101, 48);
			this.Total_Label.TabIndex = 111;
			this.Total_Label.Text = "Total: ";
			// 
			// Diagnostic_TB
			// 
			this.Diagnostic_TB.Location = new System.Drawing.Point(20, 117);
			this.Diagnostic_TB.Name = "Diagnostic_TB";
			this.Diagnostic_TB.Size = new System.Drawing.Size(346, 81);
			this.Diagnostic_TB.TabIndex = 15;
			this.Diagnostic_TB.Text = "";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label9.Location = new System.Drawing.Point(19, 204);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(116, 36);
			this.label9.TabIndex = 110;
			this.label9.Text = "Tratament*:";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label10.Location = new System.Drawing.Point(15, 78);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(120, 36);
			this.label10.TabIndex = 109;
			this.label10.Text = "Diagnostic*:";
			// 
			// Salveaza_Button
			// 
			this.Salveaza_Button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
			this.Salveaza_Button.Cursor = System.Windows.Forms.Cursors.Hand;
			this.Salveaza_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Salveaza_Button.Font = new System.Drawing.Font("Rockwell", 20F);
			this.Salveaza_Button.Location = new System.Drawing.Point(587, 619);
			this.Salveaza_Button.Name = "Salveaza_Button";
			this.Salveaza_Button.Size = new System.Drawing.Size(284, 45);
			this.Salveaza_Button.TabIndex = 20;
			this.Salveaza_Button.Text = "Salveaza";
			this.Salveaza_Button.UseVisualStyleBackColor = false;
			this.Salveaza_Button.Click += new System.EventHandler(this.Salveaza_Button_Click);
			// 
			// Radiografii_GB
			// 
			this.Radiografii_GB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Radiografii_GB.Controls.Add(this.buttonsPanel);
			this.Radiografii_GB.Controls.Add(this.AddNew_Button);
			this.Radiografii_GB.Font = new System.Drawing.Font("Rockwell", 15F);
			this.Radiografii_GB.ForeColor = System.Drawing.Color.DarkGreen;
			this.Radiografii_GB.Location = new System.Drawing.Point(1112, 64);
			this.Radiografii_GB.Name = "Radiografii_GB";
			this.Radiografii_GB.Size = new System.Drawing.Size(310, 549);
			this.Radiografii_GB.TabIndex = 18;
			this.Radiografii_GB.TabStop = false;
			this.Radiografii_GB.Text = "Radiografii";
			// 
			// buttonsPanel
			// 
			this.buttonsPanel.AutoScroll = true;
			this.buttonsPanel.Location = new System.Drawing.Point(7, 31);
			this.buttonsPanel.Name = "buttonsPanel";
			this.buttonsPanel.Size = new System.Drawing.Size(289, 465);
			this.buttonsPanel.TabIndex = 0;
			// 
			// AddNew_Button
			// 
			this.AddNew_Button.BackColor = System.Drawing.Color.White;
			this.AddNew_Button.Cursor = System.Windows.Forms.Cursors.Hand;
			this.AddNew_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.AddNew_Button.Font = new System.Drawing.Font("Rockwell", 12F);
			this.AddNew_Button.ForeColor = System.Drawing.Color.Black;
			this.AddNew_Button.Location = new System.Drawing.Point(7, 502);
			this.AddNew_Button.Name = "AddNew_Button";
			this.AddNew_Button.Size = new System.Drawing.Size(289, 32);
			this.AddNew_Button.TabIndex = 19;
			this.AddNew_Button.Text = "Add new Image";
			this.AddNew_Button.UseVisualStyleBackColor = false;
			this.AddNew_Button.Click += new System.EventHandler(this.AddNew_Button_Click);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			// 
			// Progress
			// 
			this.Progress.Location = new System.Drawing.Point(587, 670);
			this.Progress.Maximum = 10;
			this.Progress.Name = "Progress";
			this.Progress.Size = new System.Drawing.Size(284, 23);
			this.Progress.TabIndex = 114;
			this.Progress.Visible = false;
			// 
			// NouaProgramare
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1434, 705);
			this.Controls.Add(this.Progress);
			this.Controls.Add(this.Radiografii_GB);
			this.Controls.Add(this.Salveaza_Button);
			this.Controls.Add(this.Remediu_GB);
			this.Controls.Add(this.Adresa_Domiciliu);
			this.Controls.Add(this.Detalii_Personale);
			this.Controls.Add(this.label1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "NouaProgramare";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Detalii Programare Noua";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NouPacient_FormClosing);
			this.Detalii_Personale.ResumeLayout(false);
			this.Detalii_Personale.PerformLayout();
			this.Adresa_Domiciliu.ResumeLayout(false);
			this.Adresa_Domiciliu.PerformLayout();
			this.Remediu_GB.ResumeLayout(false);
			this.Remediu_GB.PerformLayout();
			this.Radiografii_GB.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox Detalii_Personale;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox CNP_TextBox;
		private System.Windows.Forms.TextBox Prenume_TextBox;
		private System.Windows.Forms.TextBox Nume_TextBox;
		private System.Windows.Forms.GroupBox Adresa_Domiciliu;
		private System.Windows.Forms.TextBox Adresa_TextBox;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox Localitatea_TextBox;
		private System.Windows.Forms.TextBox Judetul_TextBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.DateTimePicker DataProgramare;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox NrTelefon_TextBox;
		private System.Windows.Forms.GroupBox Remediu_GB;
		private System.Windows.Forms.RichTextBox Diagnostic_TB;
		private System.Windows.Forms.Label Total_Label;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Button Salveaza_Button;
		private System.Windows.Forms.GroupBox Radiografii_GB;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.ProgressBar Progress;
		private System.Windows.Forms.FlowLayoutPanel buttonsPanel;
		private System.Windows.Forms.Button AddNew_Button;
		private System.Windows.Forms.ComboBox Hour_CB;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.ComboBox Min_CB;
		private System.Windows.Forms.ComboBox Prices_CB;
		private System.Windows.Forms.FlowLayoutPanel Labels_Panel;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox Email_TextBox;
		private System.Windows.Forms.TextBox Sex_TextBox;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.FlowLayoutPanel Dinti_Panel;
		private System.Windows.Forms.ComboBox DintiAfectati_ComboBox;
		private System.Windows.Forms.Button Sterge_DintiAfectati;
		private System.Windows.Forms.Button Toti_Button;
		private System.Windows.Forms.Button StergeTratament_Button;
	}
}
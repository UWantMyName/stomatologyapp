﻿using System.Collections.Generic;

namespace StomatologyApp
{
	public class Pacient
	{
		private string Nume { get; set; }
		private string Prenume { get; set; }
		private string Sex { get; set; }
		private string CNP { get; set; }
		private string NrTelefon { get; set; }
		private string Email { get; set; }		

		private string Adresa { get; set; }
		private string Localitate { get; set; }
		private string Judetul { get; set; }

		private List<Programare> Programari { get; set; }

		public Pacient()
		{
			Programari = new List<Programare>();
		}

		public void SetNume(string nume) { this.Nume = nume; }
		public string GetNume() { return this.Nume; }

		public void SetPrenume(string prenume) { this.Prenume = prenume; }
		public string GetPrenume() { return this.Prenume; }

		public void SetSex(string sex) { this.Sex = sex; }
		public string GetSex() { return this.Sex; }

		public void SetCNP(string cnp) { this.CNP = cnp; }
		public string GetCNP() { return this.CNP; }

		public void SetNrTelefon(string nrtelefon) { this.NrTelefon = nrtelefon; }
		public string GetNrTelefon() { return this.NrTelefon; }

		public void SetEmail(string email) { this.Email = email; }
		public string GetEmail() { return this.Email; }

		public void SetAdresa(string adresa) { this.Adresa = adresa; }
		public string GetAdresa() { return this.Adresa; }

		public void SetLocalitate(string localitate) { this.Localitate = localitate; }
		public string GetLocaliltate() { return this.Localitate; }

		public void SetJudetul(string judetul) { this.Judetul = judetul; }
		public string GetJudetul() { return this.Judetul; }

		public void AddProgramare(Programare p) { Programari.Add(p); }
		public List<Programare> GetProgramari() { return this.Programari; }
	}
}

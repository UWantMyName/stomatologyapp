﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StomatologyApp
{
	public partial class NouaProgramare : Form
	{
		private List<Radiografie> list = new List<Radiografie>();
		private bool salvat_pressed = false;
		private List<ButtonPair> panelControls = new List<ButtonPair>();
		private int length = 0;

		private Dictionary<string, int> prices = new Dictionary<string, int>();
		private int total = 0;

		private List<string> tratamenteNume = new List<string>();
		private List<int> tratamentePreturi = new List<int>();
		private List<string> dintiAfectati = new List<string>();

		private bool dataSchimbata = false;
		private bool oraSchimbata = false;

		public NouaProgramare()
		{
			InitializeComponent();
			prices = Tratamente.GetPrices();
			InitTimeComboBoxes();
			InitPricesComboBox();
		}

		public NouaProgramare(Pacient p)
		{
			InitializeComponent();
			prices = Tratamente.GetPrices();
			InitTimeComboBoxes();
			InitPricesComboBox();

			Nume_TextBox.Text = p.GetNume();
			Prenume_TextBox.Text = p.GetPrenume();
			CNP_TextBox.Text = p.GetCNP();
			NrTelefon_TextBox.Text = p.GetNrTelefon();
			Email_TextBox.Text = p.GetEmail();
			Sex_TextBox.Text = p.GetSex();
			Adresa_TextBox.Text = p.GetAdresa();
			Localitatea_TextBox.Text = p.GetLocaliltate();
			Judetul_TextBox.Text = p.GetJudetul();
		}

		private void InitTimeComboBoxes()
		{
			// hour
			for (int i = 0; i < 10; i++)
				Hour_CB.Items.Add("0" + i.ToString());
			for (int i = 10; i < 24; i++)
				Hour_CB.Items.Add(i.ToString());

			// minute
			for (int i = 0; i < 10; i++)
				Min_CB.Items.Add("0" + i.ToString());
			for (int i = 10; i < 60; i++)
				Min_CB.Items.Add(i.ToString());
		}

		private void InitPricesComboBox()
		{
			int i = 0;
			foreach (KeyValuePair<string, int> k in prices)
			{
				i++;
				Prices_CB.Items.Add(i.ToString() + ") " + k.Key);
			}
		}

		private bool CheckDetaliiPersonale()
		{
			if (Nume_TextBox.Text.Length == 0) return false;
			if (Prenume_TextBox.Text.Length == 0) return false;
			if (CNP_TextBox.Text.Length == 0) return false;
			if (NrTelefon_TextBox.Text.Length == 0) return false;
			if (Sex_TextBox.Text.Length == 0) return false;			
			return true;
		}

		private bool CheckAdresaDomiciliu()
		{			
			if (Adresa_TextBox.Text.Length == 0) return false;
			if (Localitatea_TextBox.Text.Length == 0) return false;
			if (Judetul_TextBox.Text.Length == 0) return false;
			return true;
		}

		/// <summary>
		/// Disables all the controls so that the user cannot do some hamrful actions that could give a fatal error
		/// when inserting a new Pacient into the database.
		/// </summary>
		private void DisableAllControls()
		{
			Detalii_Personale.Enabled = false;
			Adresa_Domiciliu.Enabled = false;
			Remediu_GB.Enabled = false;
			Radiografii_GB.Enabled = false;
			Salveaza_Button.Enabled = false;
		}

		private void EnableAllControls()
		{
			Detalii_Personale.Enabled = true;
			Adresa_Domiciliu.Enabled = true;
			Remediu_GB.Enabled = true;
			Radiografii_GB.Enabled = true;
			Salveaza_Button.Enabled = true;
		}

		[Obsolete]
		private void Salveaza_Button_Click(object sender, EventArgs e)
		{
			if (CheckDetaliiPersonale() && CheckAdresaDomiciliu())
			{
				salvat_pressed = true;

				DisableAllControls();
				Progress.Visible = true;

				Pacient p = new Pacient();

				p.SetNume(Nume_TextBox.Text);
				p.SetPrenume(Prenume_TextBox.Text);
				p.SetSex(Sex_TextBox.Text);
				p.SetCNP(CNP_TextBox.Text);
				p.SetNrTelefon(NrTelefon_TextBox.Text);
				p.SetEmail(Email_TextBox.Text);
				p.SetAdresa(Adresa_TextBox.Text);
				p.SetLocalitate(Localitatea_TextBox.Text);
				p.SetJudetul(Judetul_TextBox.Text);

				Programare prog = new Programare();
				prog.SetDataProgramat(DataProgramare.Value);
				prog.SetOraProgramat(Hour_CB.Text, Min_CB.Text);
				prog.SetDiagnostic(Diagnostic_TB.Text);
				prog.SetTratamente(tratamenteNume, tratamentePreturi);
				prog.SetPlatit(false);
				List<Radiografie> actualList = new List<Radiografie>();
				foreach(Radiografie r in list)
				{
					if (!r.IsEmpty()) actualList.Add(r);
				}
				prog.SetRadiografii(actualList);
				prog.SetDintiAfectati(dintiAfectati);

				p.AddProgramare(prog);

				if (!File.Exists(Directory.GetCurrentDirectory() + "/path.stapp"))
				{
					string errorMess = "Nu gasesc sau nu pot folosi locatia unde salvezi datele! Nu pot salva date!\n\nPoate nu ai setat locatia unde salvezi datele?";

					MessageBox.Show(errorMess, "EROARE!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					salvat_pressed = false;

					EnableAllControls();
					Progress.Visible = false;
				}
				else
				{
					try
					{
						DataManagement.SalveazaPacient(p, Progress);
						this.Close();
					}
					catch (SqlException exc)
					{
						MessageBox.Show(exc.ToString());
					}
				}

				
			}
			else MessageBox.Show("Prea putine detalii.");
		}

		private void AddRadiografie(int index, object sender)
		{
			DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
			if (result == DialogResult.OK) // Test result.
			{
				string path = openFileDialog1.FileName;
				Button b = sender as Button;
				string[] parts = path.Split('\\'); // just so I can put the name of the file on the button
				
				Image img = Image.FromFile(path);
				Radiografie rad = new Radiografie(parts[parts.Length - 1], img);
				Date dt = new Date();
				dt.SetZi(DataProgramare.Value.Day);
				dt.SetLuna(DataProgramare.Value.Month);
				dt.SetAn(DataProgramare.Value.Year);
				rad.SetDataEfectuata(dt);

				// checking if the same Radiografie exists within the list
				bool duplicate = false;
				for (int i = 0; i < list.Count && !duplicate; i++)
					if (list[i].GetName() == rad.GetName())
						duplicate = true;


				if (!duplicate)
				{
					list[index] = rad;
					b.Text = parts[parts.Length - 1];
				}
				else MessageBox.Show("Imaginea este deja selectata in lista.");
			}
		}

		/// <summary>
		/// Gets the date the image was taken.
		/// WARNING: It may not be specified, which causes exceptions to arrise!
		/// </summary>
		/// <param name="image"></param>
		/// <returns></returns>
		//private Date GetDate(Image image)
		//{
		//	PropertyItem propItem = image.PropertyItems.FirstOrDefault(i => i.Id == 0x132);
		//	if (propItem != null)
		//	{
		//		// Extract the property value as a String. 
		//		ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
		//		string text = encoding.GetString(propItem.Value, 0, propItem.Len - 1);

		//		// Parse the date and time. 
		//		CultureInfo provider = CultureInfo.InvariantCulture;
		//		DateTime dateCreated = DateTime.ParseExact(text, "yyyy:MM:d H:m:s", provider);
		//		int date = int.Parse(dateCreated.Day.ToString());
		//		int month = int.Parse(dateCreated.Month.ToString());
		//		int year = int.Parse(dateCreated.Year.ToString());
		//		Date dt = new Date(date, month, year);

		//		return dt;
		//	}
		//	return null;
		//}

		private void ChooseFile_Click(object sender, EventArgs e)
		{
			Button b = sender as Button;
			int index;
			bool found = false;
			for (index = 0; index < panelControls.Count && !found; index++)
				if (panelControls[index].Exists(b.Name))
					found = true;
			AddRadiografie(index - 1, sender);
		}

		private void DeleteButton_Click(object sender, EventArgs e)
		{
			Button b = sender as Button;

			// Find the index of the sender object in the list of controls
			bool found = false;
			int i;
			for (i = 0; i < panelControls.Count && !found; i++)
				if (panelControls[i].Exists(b.Name))
					found = true;
			
			i--;
			// Delete the radiografie from the list
			if (i < list.Count) list.RemoveAt(i);
			// Delete the buttons from the panel
			ButtonPair pair = panelControls[i];
			buttonsPanel.Controls.Remove(pair.GetChooseFileButton());
			buttonsPanel.Controls.Remove(pair.GetDeleteFileButton());
			// Delete the buttons from the panelControls
			panelControls.RemoveAt(i);
		}

		private void AddNew_Button_Click(object sender, EventArgs e)
		{		
			Button chooseFile = new Button();
			chooseFile.Name = "CF_" + length.ToString();
			chooseFile.Cursor = Cursors.Hand;
			chooseFile.Size = new Size(207, 32);
			chooseFile.Text = "Choose File";
			chooseFile.Font = new Font("Rockwell", 12);
			chooseFile.BackColor = Color.White;
			chooseFile.ForeColor = Color.Black;
			chooseFile.FlatStyle = FlatStyle.Popup;

			Button deleteFile = new Button();
			deleteFile.Name = "DF_" + length.ToString();
			deleteFile.Cursor = Cursors.Hand;
			deleteFile.Size = new Size(38, 34);
			deleteFile.Text = "X";
			deleteFile.Font = new Font("Rockwell", 18);
			deleteFile.BackColor = Color.White;
			deleteFile.ForeColor = Color.Red;
			deleteFile.FlatStyle = FlatStyle.Popup;

			length++;

			chooseFile.Click += new EventHandler(this.ChooseFile_Click);
			deleteFile.Click += new EventHandler(this.DeleteButton_Click);

			ButtonPair pair = new ButtonPair(chooseFile, deleteFile);
			panelControls.Add(pair);

			list.Add(new Radiografie());

			buttonsPanel.Controls.Add(chooseFile);
			buttonsPanel.Controls.Add(deleteFile);
		}

		private void NouPacient_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!salvat_pressed)
			{
				string message = "Esti sigur ca vrei sa inchizi fara sa salvezi? Vei pierde detaliile pacientului!";
				string title = "Avertisment!!";
				MessageBoxButtons buttons = MessageBoxButtons.YesNo;
				DialogResult result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);

				if (result == DialogResult.Yes) e.Cancel = false;
				else e.Cancel = true;
			}
		}

		private void Prices_CB_SelectedValueChanged(object sender, EventArgs e)
		{
			string str = Prices_CB.Text;
			int index = 0;

			while (!('a' <= str[index] && str[index] <= 'z') || ('A' <= str[index] && str[index] <= 'Z'))
				index++;

			string str2 = "";
			for (int i = index - 1; i < str.Length; i++)
				str2 += str[i];

			total += prices[str2];

			Total_Label.Text = "Total: " + total.ToString() + " RON";
			tratamenteNume.Add(str2);
			tratamentePreturi.Add(prices[str2]);

			Label l = new Label();
			l.Size = new Size(700, 24);
			l.Font = new Font("Myanmar Text", 15, FontStyle.Bold);
			l.ForeColor = Color.DarkCyan;
			l.Text = str2;
			l.Text += " --- " + prices[str2].ToString();
			Labels_Panel.Controls.Add(l);
		}

		private void DintiAfectati_ComboBox_SelectedValueChanged(object sender, EventArgs e)
		{
			Label l = new Label();
			l.Size = new Size(700, 24);
			l.Font = new Font("Myanmar Text", 15);
			l.ForeColor = Color.Red;
			l.Text = DintiAfectati_ComboBox.Text;
			dintiAfectati.Add(DintiAfectati_ComboBox.Text);
			Dinti_Panel.Controls.Add(l);
		}

		private void Sterge_DintiAfectati_Click(object sender, EventArgs e)
		{
			if (dintiAfectati.Count() > 0)
			{
				dintiAfectati.RemoveAt(dintiAfectati.Count() - 1);
				Dinti_Panel.Controls.RemoveAt(Dinti_Panel.Controls.Count - 1);
			}
		}

		private void Toti_Button_Click(object sender, EventArgs e)
		{
			Label l = new Label();
			l.Size = new Size(700, 24);
			l.Font = new Font("Myanmar Text", 15);
			l.ForeColor = Color.Red;
			l.Text = "Toti dintii";
			dintiAfectati.Add("Toti dintii");
			Dinti_Panel.Controls.Add(l);
		}

		private void StergeTratament_Button_Click(object sender, EventArgs e)
		{
			if (tratamenteNume.Count > 0)
			{
				Labels_Panel.Controls.RemoveAt(Labels_Panel.Controls.Count - 1);
				total -= tratamentePreturi[tratamentePreturi.Count - 1];
				tratamenteNume.RemoveAt(tratamenteNume.Count - 1);
				tratamentePreturi.RemoveAt(tratamentePreturi.Count - 1);
			}
		}

		private void Hour_CB_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (Hour_CB.Text.Length > 0)
			{
				oraSchimbata = true;
			}
			else
			{
				oraSchimbata = false;
			}
		}

		private void Min_CB_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (Min_CB.Text.Length > 0)
			{
				oraSchimbata = true;
			}
			else
			{
				oraSchimbata = false;
			}
		}

		private void DataProgramare_ValueChanged(object sender, EventArgs e)
		{
			dataSchimbata = true;
		}
	}
}

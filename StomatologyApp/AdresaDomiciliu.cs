﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StomatologyApp
{
	public class AdresaDomiciliu
	{
		private string strada;
		private int bloc;
		private string localitatea;
		private string judetul;

		/// <summary>
		/// Main constructor for AdresaBloc
		/// </summary>
		public AdresaDomiciliu() { }

		/// <summary>
		/// Setter for Strada.
		/// </summary>
		/// <param name="strada"> Strada of adresa. </param>
		public void SetStrada(string strada) { this.strada = strada; }
		
		/// <summary>
		/// Setter for Bloc.
		/// </summary>
		/// <param name="bloc"> Bloc of adresa. </param>
		public void SetBloc(int bloc) { this.bloc = bloc; }

		/// <summary>
		/// Setter for Localitate.
		/// </summary>
		/// <param name="localitate"></param>
		public void SetLocalitate(string localitate) { this.localitatea = localitate; }

		/// <summary>
		/// Setter for Judetul.
		/// </summary>
		/// <param name="judetul"></param>
		public void SetJudetul(string judetul) { this.judetul = judetul; }


		/// <summary>
		/// Getter of Strada.
		/// </summary>
		/// <returns> Strada of Adresa. </returns>
		public string GetStrada() { return this.strada; }

		/// <summary>
		/// Getter of Bloc.
		/// </summary>
		/// <returns> Bloc of Adresa. </returns>
		public int GetBloc() { return this.bloc; }

		/// <summary>
		/// Getter for Localitate.
		/// </summary>
		/// <returns></returns>
		public string GetLocalitate() { return this.localitatea; }

		/// <summary>
		/// Getter for Judetul.
		/// </summary>
		/// <returns></returns>
		public string GetJudetul() { return this.judetul; }

		/// <summary>
		/// Returns a string representation of AdresaBloc.
		/// </summary>
		/// <returns> String representation of AdresaBloc. </returns>
		public override string ToString()
		{
			return this.strada + ", " + this.bloc + ", " + this.localitatea + ", " + this.judetul;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StomatologyApp
{
	public partial class DetaliiPacientSelectat : Form
	{
		private bool editable = false;
		private List<Radiografie> list = new List<Radiografie>();
		int nrPreviousButtons = 0;
		private List<ButtonPair> panelControls = new List<ButtonPair>();
		private bool salvat_pressed = false;

		private string[] folders;
		private Pacient pacient;

		public DetaliiPacientSelectat(Pacient p, string path)
		{
			InitializeComponent();
			this.pacient = p;
			InitForm(p, path);
		}

		private void InitForm(Pacient p, string path)
		{
			Nume_TextBox.Text = p.GetNume();
			Prenume_TextBox.Text = p.GetPrenume();
			this.Text = "Detalii " + Nume_TextBox.Text + " " + Prenume_TextBox.Text;
			CNP_TextBox.Text = p.GetCNP();
			NrTelefon_TextBox.Text = p.GetNrTelefon();
			Email_TextBox.Text = p.GetEmail();
			Sex_TextBox.Text = p.GetSex();

			Adresa_TextBox.Text = p.GetAdresa();
			Localitatea_TextBox.Text = p.GetLocaliltate();
			Judetul_TextBox.Text = p.GetJudetul();


			// Now, I need to create buttons that redirect me to the details of a specific Programare
			folders = Directory.GetDirectories(path);
			int tab_index = 1;
			foreach(string s in folders)
			{
				string[] parts = s.Split('\\');
				string foldername = parts[parts.Length - 1];

				Button b = new Button();
				b.Name = "button" + tab_index.ToString();
				b.Text = foldername;
				b.Font = new Font("Rockwell", 12);
				b.Cursor = Cursors.Hand;
				b.BackColor = Color.White;
				b.ForeColor = Color.OrangeRed;
				b.Size = new Size(365, 32);
				b.Visible = true;
				b.TabIndex = tab_index;
				b.Click += new EventHandler(ProgramareButton_Click);
				tab_index++;
				buttonsPanel.Controls.Add(b);
			}
		}

		#region Methods for the editing mechanism
		private void ShowImages(Pacient p)
		{
			//ListaRadiografii lr = p.GetListaradiografii();
			ListaRadiografii lr;
			//list = lr.GetLista();
			foreach (Radiografie r in list)
			{
				ExportImage(r);

				Button b = new Button();
				b.BackColor = Color.White;
				b.Cursor = Cursors.Hand;
				b.FlatStyle = FlatStyle.Popup;
				b.Font = new Font("Rockwell", 12);
				b.ForeColor = Color.Black;
				b.Size = new Size(330, 32);
				b.Text = r.GetName();
				b.Click += new EventHandler(this.OpenImage);
				DetailsPanel.Controls.Add(b);
				nrPreviousButtons++;
			}
		}

		private void ExportImage(Radiografie r)
		{
			string location = "temp";
			string name = r.GetName();
			Image data = r.GetRadiografie();

			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			documentsPath = Path.Combine(documentsPath, "StomatologyApp - Temporary Files", location);
			Directory.CreateDirectory(documentsPath);

			string filePath = Path.Combine(documentsPath, name);
			byte[] bArray = ImageConverter.ImageToByteArray(data);
			Bitmap b = new Bitmap(data);
			b.Save(filePath, ImageFormat.Jpeg);
		}

		private void OpenImage(object sender, EventArgs e)
		{
			Button b = sender as Button;
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			documentsPath = Path.Combine(documentsPath, "StomatologyApp - Temporary Files", "temp");
			string filePath = Path.Combine(documentsPath, b.Text);

			System.Diagnostics.Process.Start(filePath);
		}

		private void Editeaza_Button_Click(object sender, EventArgs e)
		{
			//editable = !editable;
			//Nume_TB.ReadOnly = !editable;
			//Prenume_TB.ReadOnly = !editable;
			//CNP_TB.ReadOnly = !editable;
			//NRT_TB.ReadOnly = !editable;
			//Strada_TB.ReadOnly = !editable;
			//Bloc_TB.ReadOnly = !editable;
			//L_TB.ReadOnly = !editable;
			//J_TB.ReadOnly = !editable;
			//DataProg_TB.ReadOnly = !editable;
			//Efectuat_TB.ReadOnly = !editable;
			//Diagnostic_TB.ReadOnly = !editable;
			//Tratament_TB.ReadOnly = !editable;
			//Cost_TB.ReadOnly = !editable;
			//Plata_TB.ReadOnly = !editable;
			//RadiografieNoua.Visible = editable;
		}

		private void Salveaza_Button_Click(object sender, EventArgs e)
		{
			salvat_pressed = true;

			if (editable)
			{
				Pacient p = new Pacient();
				DetaliiPersonale dp = new DetaliiPersonale();
				AdresaDomiciliu ad = new AdresaDomiciliu();
				Programare pr = new Programare();
				Remediu rm = new Remediu();

				//dp.SetNume(Nume_TB.Text);
				//dp.SetPrenume(Prenume_TB.Text);
				//dp.SetCNP(CNP_TB.Text);
				//dp.SetNrTelefon(NRT_TB.Text);

				//ad.SetStrada(Strada_TB.Text);
				//ad.SetBloc(int.Parse(Bloc_TB.Text));
				//ad.SetLocalitate(L_TB.Text);
				//ad.SetJudetul(J_TB.Text);

				//pr.SetProgramat(DataProg_TB.Text);
				//pr.SetEfectuat(Efectuat_TB.Text);

				//rm.SetDiagnostic(Diagnostic_TB.Text);
				//rm.SetTratament(Tratament_TB.Text);
				//rm.SetCost(float.Parse(Cost_TB.Text));
				//rm.SetPlata(float.Parse(Plata_TB.Text));

				//p.SetDetaliiPersonale(dp);
				//p.SetAdresaDomiciliu(ad);
				//p.SetProgramare(pr);
				//p.SetRemediu(rm);

				List<Radiografie> lista = new List<Radiografie>();
				for (int i = nrPreviousButtons; i < list.Count; i++)
					if (list[i] != null) lista.Add(list[i]);

				try
				{
					//DatabaseController.UpdatePacient(p, ID_Pacient, lista);
					MessageBox.Show("Pacient actualizat cu succes!");
					this.Close();
				}
				catch (Exception exc)
				{
					MessageBox.Show("Eroare la salvarea pacientului. Nu s-a salvat nimic inca.\n" + exc.ToString());
				}
			}
			else this.Close();
		}

		private void RadiografieNoua_Click(object sender, EventArgs e)
		{
			Button chooseFile = new Button();
			chooseFile.Cursor = Cursors.Hand;
			chooseFile.Size = new Size(280, 30);
			chooseFile.Text = "Choose File";
			chooseFile.BackColor = Color.White;
			chooseFile.Font = new Font("Rockwell", 15);
			chooseFile.FlatStyle = FlatStyle.Popup;

			Button deleteFile = new Button();
			deleteFile.Cursor = Cursors.Hand;
			deleteFile.Size = new Size(40, 30);
			deleteFile.Text = "X";
			deleteFile.Font = new Font("Rockwell", 15);
			deleteFile.BackColor = Color.White;
			deleteFile.ForeColor = Color.Red;
			deleteFile.FlatStyle = FlatStyle.Popup;

			chooseFile.Click += new EventHandler(this.ChooseFile_Click);
			deleteFile.Click += new EventHandler(this.DeleteButton_Click);

			DetailsPanel.Controls.Add(chooseFile);
			DetailsPanel.Controls.Add(deleteFile);
		}

		private Date GetDate(Image image)
		{
			PropertyItem propItem = image.PropertyItems.FirstOrDefault(i => i.Id == 0x132);
			if (propItem != null)
			{
				// Extract the property value as a String. 
				ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
				string text = encoding.GetString(propItem.Value, 0, propItem.Len - 1);

				// Parse the date and time. 
				CultureInfo provider = CultureInfo.InvariantCulture;
				DateTime dateCreated = DateTime.ParseExact(text, "yyyy:MM:d H:m:s", provider);
				int date = int.Parse(dateCreated.Day.ToString());
				int month = int.Parse(dateCreated.Month.ToString());
				int year = int.Parse(dateCreated.Year.ToString());
				Date dt = new Date(date, month, year);

				return dt;
			}
			return null;
		}

		private void AddRadiografie(int index, object sender)
		{
			DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
			if (result == DialogResult.OK) // Test result.
			{
				string path = openFileDialog1.FileName;
				Button b = sender as Button;
				string[] parts = path.Split('\\'); // just so I can put the name of the file on the button
				b.Text = parts[parts.Length - 1];

				Image img = Image.FromFile(path);
				Radiografie rad = new Radiografie(b.Text, img);
				Date dt = GetDate(rad.GetRadiografie());
				rad.SetDataEfectuata(dt);
				list.Add(rad);
			}
		}

		private void ChooseFile_Click(object sender, EventArgs e)
		{
			Button b = sender as Button;
			int index;
			bool found = false;
			for (index = 0; index < panelControls.Count && !found; index++)
				if (panelControls[index].Exists(b.Name))
					found = true;
			AddRadiografie(index - 1, sender);
		}

		private void DeleteButton_Click(object sender, EventArgs e)
		{
			Button b = sender as Button;

			// Find the index of the sender object in the list of controls
			bool found = false;
			int i;
			for (i = 0; i < panelControls.Count && !found; i++)
				if (panelControls[i].Exists(b.Name))
					found = true;

			i--;
			MessageBox.Show("Removed the index " + i.ToString());
			// Delete the radiografie from the list
			if (i < list.Count) list.RemoveAt(nrPreviousButtons - 1 + i);
			// Delete the buttons from the panel
			ButtonPair pair = panelControls[i];
			DetailsPanel.Controls.Remove(pair.GetChooseFileButton());
			DetailsPanel.Controls.Remove(pair.GetDeleteFileButton());
			// Delete the buttons from the panelControls
			panelControls.RemoveAt(i);
		}

		private void DetaliiPacientSelectat_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!salvat_pressed && editable)
			{
				string message = "Esti sigur ca vrei sa inchizi fara sa salvezi? Vei pierde detaliile pacientului!";
				string title = "Avertisment!!";
				MessageBoxButtons buttons = MessageBoxButtons.YesNo;
				DialogResult result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);

				if (result == DialogResult.Yes) e.Cancel = false;
				else e.Cancel = true;
			}
		}

#endregion

		private void ProgramareButton_Click(object sender, EventArgs e)
		{
			Button b = sender as Button;
			Programare prog = new Programare();
			using (StreamReader read = new StreamReader(folders[b.TabIndex - 1] + "\\" + "DateProgramare.stapp"))
			{
				// date
				string line = read.ReadLine();
				string[] parts = line.Split(' ');
				string zi = parts[parts.Length - 3];
				string an = parts[parts.Length - 1];
				string luna = parts[parts.Length - 2];
				int i;

				Date dt = new Date(zi, luna, an);
				prog.SetDataProgramat(dt);

				// hour
				line = read.ReadLine();
				string hour = "";
				string minute = "";
				i = line.IndexOf(":") + 2;
				while ('0' <= line[i] && line[i] <= '9')
				{
					hour += line[i];
					i++;
				}
				i++;
				while ('0' <= line[i] && line[i] <= '9')
				{
					minute += line[i];
					i++;
				}

				prog.SetOraProgramat(hour, minute);

				// diagnostic
				line = read.ReadLine();
				string diagnostic = "";
				for (i = line.IndexOf(':') + 2; i < line.Length; i++)
					diagnostic += line[i];

				line = read.ReadLine();
				while (line.IndexOf(':') == -1)
				{
					diagnostic += Environment.NewLine;
					diagnostic += line;
					line = read.ReadLine();
				}
				prog.SetDiagnostic(diagnostic);

				// dinti afectati
				line = read.ReadLine();
				parts = line.Split(',');
				List<string> da = new List<string>();
				foreach (string str in parts)
					da.Add(str);

				prog.SetDintiAfectati(da);

				// tratamente
				read.ReadLine();
				List<string> tratamenteNume = new List<string>();
				List<int> tratamentePreturi = new List<int>();
				line = read.ReadLine();
				while(!line.Contains("Platit"))
				{
					string[] ps = line.Split('-');
					tratamenteNume.Add(ps[0]);
					tratamentePreturi.Add(int.Parse(ps[ps.Length - 1]));
					line = read.ReadLine();
				}
				prog.SetTratamente(tratamenteNume, tratamentePreturi);

				// platit
				// line is already read from the previous section
				if (line.Contains("DA"))
				{
					prog.SetPlatit(true);
					// need to set the date
					line = read.ReadLine();
					string[] part = line.Split(' ');
					Date platit = new Date(part[0], part[1], part[2]);
					prog.SetDataPlatit(platit);
				}
				else
				{
					prog.SetPlatit(false);
				}

				// pacient prezentat
				line = read.ReadLine();
				if (line.Contains("DA")) prog.SetPrezentat("DA");
				else if (line.Contains("NU")) prog.SetPrezentat("NU");
				else prog.SetPrezentat("neprecizat");
				
			}

			VeziProgramare f = new VeziProgramare(pacient, prog, folders[b.TabIndex - 1]);
			f.Show();
		}

		private void AddNew_Button_Click(object sender, EventArgs e)
		{
			NouaProgramare f = new NouaProgramare(pacient);
			f.Show();
		}
	}
}

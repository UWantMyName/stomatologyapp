﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StomatologyApp
{
	public partial class AlegeAtribute : Form
	{
		private readonly bool[] checkedRB = { true, true, true, true, true, true, true, true, true, true, true, true, true, true };

		public AlegeAtribute()
		{
			InitializeComponent();
			ReadData();
			InitRB();
		}

		/// <summary>
		/// Reads the data so it knows which attributes are selected to show in the DataGridView
		/// </summary>
		private void ReadData()
		{
			if (File.Exists("RB.stapp"))
			{
				using (StreamReader read = new StreamReader("RB.stapp"))
				{
					string line;

					for (int i = 0; i < 14; i++)
					{
						line = read.ReadLine();
						if (line == "1") checkedRB[i] = true;
						else checkedRB[i] = false;
					}

					read.Dispose();
				}
			}
			else
			{
				File.Create("RB.stapp");
				WriteData();
			}
		}

		/// <summary>
		/// Writes data to a file so that the app knows the preferences of the user.
		/// </summary>
		private void WriteData()
		{
			using (StreamWriter write = new StreamWriter("RB.stapp"))
			{
				for (int i = 0; i < 14; i++)
					if (checkedRB[i]) write.WriteLine("1");
					else write.WriteLine("0");

				write.Dispose();
			}
		}

		/// <summary>
		/// Sets the RadioButtons to their checked value based on the values read
		/// </summary>
		private void InitRB()
		{
			foreach (CheckBox cb in GetAll(this, typeof(CheckBox)))
				ChangeColorToSelected(cb);
		}

		/// <summary>
		/// Returns a list of all the items with the specified type within the specified form.
		/// </summary>
		/// <param name="control"> The form in which the controls are located </param>
		/// <param name="type"> The type of the object to get. Use typeof(Type) </param>
		/// <returns></returns>
		public IEnumerable<Control> GetAll(Control control, Type type)
		{
			var controls = control.Controls.Cast<Control>();

			return controls.SelectMany(ctrl => GetAll(ctrl, type))
									  .Concat(controls)
									  .Where(c => c.GetType() == type);
		}

		/// <summary>
		/// Method that changes the background color of the checkbox based if it is checked or not.
		/// </summary>
		/// <param name="cb"> The checkbox I need to change the color of </param>
		private void ChangeColorToSelected(CheckBox cb)
		{
			int index = int.Parse(cb.Tag.ToString());
			if (checkedRB[index]) cb.BackColor = Color.LimeGreen;
			else cb.BackColor = Color.LightGray;
		}

		#region Button Controls Handlers

		private void OKButton_Click(object sender, EventArgs e)
		{
			WriteData();
			this.Close();
		}

		private void Toate_Button_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < 14; i++)
				checkedRB[i] = true;

			foreach (CheckBox cb in GetAll(this, typeof(CheckBox)))
				ChangeColorToSelected(cb);
		}

		#endregion

		#region Attribute Button Handlers

		private void Nume_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(Nume_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(Nume_CB);
		}

		private void Prenume_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(Prenume_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(Prenume_CB);
		}

		private void CNP_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(CNP_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(CNP_CB);
		}

		private void NrTelefon_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(NrTelefon_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(NrTelefon_CB);
		}

		private void StradaDomiciliu_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(StradaDomiciliu_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(StradaDomiciliu_CB);
		}

		private void BlocDomiciliu_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(BlocDomiciliu_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(BlocDomiciliu_CB);
		}

		private void Localitatea_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(Localitatea_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(Localitatea_CB);
		}

		private void Judetul_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(Judetul_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(Judetul_CB);
		}

		private void ProgramatLa_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(ProgramatLa_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(ProgramatLa_CB);
		}

		private void EfectuatLa_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(EfectuatLa_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(EfectuatLa_CB);
		}

		private void Diagnostic_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(Diagnostic_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(Diagnostic_CB);
		}

		private void Tratament_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(Tratament_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(Tratament_CB);
		}

		private void Cost_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(Cost_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(Cost_CB);
		}

		private void Plata_CB_CheckedChanged(object sender, EventArgs e)
		{
			int index = int.Parse(Plata_CB.Tag.ToString());
			checkedRB[index] = !checkedRB[index];
			ChangeColorToSelected(Plata_CB);
		}

		#endregion
	}
}

﻿
namespace StomatologyApp
{
	partial class ExportDate
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportDate));
			this.label1 = new System.Windows.Forms.Label();
			this.Password_TextBox = new System.Windows.Forms.TextBox();
			this.Execute_Button = new System.Windows.Forms.Button();
			this.Path_Button = new System.Windows.Forms.Button();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Rockwell", 25F);
			this.label1.Location = new System.Drawing.Point(12, 65);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(125, 38);
			this.label1.TabIndex = 40;
			this.label1.Text = "Parola:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Password_TextBox
			// 
			this.Password_TextBox.Font = new System.Drawing.Font("Rockwell", 20F);
			this.Password_TextBox.Location = new System.Drawing.Point(143, 65);
			this.Password_TextBox.Name = "Password_TextBox";
			this.Password_TextBox.PasswordChar = '*';
			this.Password_TextBox.Size = new System.Drawing.Size(346, 39);
			this.Password_TextBox.TabIndex = 39;
			// 
			// Execute_Button
			// 
			this.Execute_Button.BackColor = System.Drawing.Color.White;
			this.Execute_Button.Cursor = System.Windows.Forms.Cursors.Hand;
			this.Execute_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Execute_Button.Font = new System.Drawing.Font("Rockwell", 12F);
			this.Execute_Button.ForeColor = System.Drawing.Color.Black;
			this.Execute_Button.Location = new System.Drawing.Point(12, 122);
			this.Execute_Button.Name = "Execute_Button";
			this.Execute_Button.Size = new System.Drawing.Size(478, 32);
			this.Execute_Button.TabIndex = 38;
			this.Execute_Button.Text = "Executa";
			this.Execute_Button.UseVisualStyleBackColor = false;
			this.Execute_Button.Click += new System.EventHandler(this.Execute_Button_Click);
			// 
			// Path_Button
			// 
			this.Path_Button.BackColor = System.Drawing.Color.White;
			this.Path_Button.Cursor = System.Windows.Forms.Cursors.Hand;
			this.Path_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Path_Button.Font = new System.Drawing.Font("Rockwell", 12F);
			this.Path_Button.ForeColor = System.Drawing.Color.Black;
			this.Path_Button.Location = new System.Drawing.Point(12, 12);
			this.Path_Button.Name = "Path_Button";
			this.Path_Button.Size = new System.Drawing.Size(478, 32);
			this.Path_Button.TabIndex = 36;
			this.Path_Button.Text = "Click pentru a preciza unde sa se salveze fisierele";
			this.Path_Button.UseVisualStyleBackColor = false;
			this.Path_Button.Click += new System.EventHandler(this.Path_Button_Click);
			// 
			// ExportDate
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(507, 166);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.Password_TextBox);
			this.Controls.Add(this.Execute_Button);
			this.Controls.Add(this.Path_Button);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ExportDate";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ExportDate";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox Password_TextBox;
		private System.Windows.Forms.Button Execute_Button;
		private System.Windows.Forms.Button Path_Button;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
	}
}
﻿
namespace StomatologyApp
{
	partial class VeziProgramare
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Remediu_GB = new System.Windows.Forms.GroupBox();
			this.PacientPrezentat_CB = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.DataPlatit = new System.Windows.Forms.DateTimePicker();
			this.Platit_RB = new System.Windows.Forms.RadioButton();
			this.Total_TB = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.Tratament_TB = new System.Windows.Forms.RichTextBox();
			this.Dinti_TB = new System.Windows.Forms.RichTextBox();
			this.Ora_TextBox = new System.Windows.Forms.TextBox();
			this.Data_TextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.Diagnostic_TB = new System.Windows.Forms.RichTextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.buttonsPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.Salveaza_Button = new System.Windows.Forms.Button();
			this.Remediu_GB.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// Remediu_GB
			// 
			this.Remediu_GB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Remediu_GB.Controls.Add(this.PacientPrezentat_CB);
			this.Remediu_GB.Controls.Add(this.label4);
			this.Remediu_GB.Controls.Add(this.label3);
			this.Remediu_GB.Controls.Add(this.DataPlatit);
			this.Remediu_GB.Controls.Add(this.Platit_RB);
			this.Remediu_GB.Controls.Add(this.Total_TB);
			this.Remediu_GB.Controls.Add(this.label2);
			this.Remediu_GB.Controls.Add(this.Tratament_TB);
			this.Remediu_GB.Controls.Add(this.Dinti_TB);
			this.Remediu_GB.Controls.Add(this.Ora_TextBox);
			this.Remediu_GB.Controls.Add(this.Data_TextBox);
			this.Remediu_GB.Controls.Add(this.label1);
			this.Remediu_GB.Controls.Add(this.label6);
			this.Remediu_GB.Controls.Add(this.label5);
			this.Remediu_GB.Controls.Add(this.Diagnostic_TB);
			this.Remediu_GB.Controls.Add(this.label9);
			this.Remediu_GB.Controls.Add(this.label10);
			this.Remediu_GB.Font = new System.Drawing.Font("Rockwell", 15F);
			this.Remediu_GB.ForeColor = System.Drawing.Color.DarkGreen;
			this.Remediu_GB.Location = new System.Drawing.Point(12, 12);
			this.Remediu_GB.Name = "Remediu_GB";
			this.Remediu_GB.Size = new System.Drawing.Size(438, 631);
			this.Remediu_GB.TabIndex = 12;
			this.Remediu_GB.TabStop = false;
			this.Remediu_GB.Text = "Detalii principale";
			// 
			// PacientPrezentat_CB
			// 
			this.PacientPrezentat_CB.FormattingEnabled = true;
			this.PacientPrezentat_CB.Items.AddRange(new object[] {
            "DA",
            "NU"});
			this.PacientPrezentat_CB.Location = new System.Drawing.Point(243, 526);
			this.PacientPrezentat_CB.Name = "PacientPrezentat_CB";
			this.PacientPrezentat_CB.Size = new System.Drawing.Size(121, 30);
			this.PacientPrezentat_CB.TabIndex = 133;
			this.PacientPrezentat_CB.Text = "Alege...";
			this.PacientPrezentat_CB.SelectedIndexChanged += new System.EventHandler(this.PacientPrezentat_CB_SelectedIndexChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label4.Location = new System.Drawing.Point(10, 526);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(226, 36);
			this.label4.TabIndex = 132;
			this.label4.Text = "S-a prezentat pacientul?";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label3.Location = new System.Drawing.Point(105, 574);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(111, 36);
			this.label3.TabIndex = 131;
			this.label3.Text = "Pe data de:";
			this.label3.Visible = false;
			// 
			// DataPlatit
			// 
			this.DataPlatit.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.DataPlatit.Location = new System.Drawing.Point(222, 574);
			this.DataPlatit.Name = "DataPlatit";
			this.DataPlatit.Size = new System.Drawing.Size(148, 31);
			this.DataPlatit.TabIndex = 130;
			this.DataPlatit.Value = new System.DateTime(2021, 1, 1, 0, 0, 0, 0);
			this.DataPlatit.Visible = false;
			this.DataPlatit.ValueChanged += new System.EventHandler(this.DataPlatit_ValueChanged);
			// 
			// Platit_RB
			// 
			this.Platit_RB.AutoSize = true;
			this.Platit_RB.ForeColor = System.Drawing.Color.Red;
			this.Platit_RB.Location = new System.Drawing.Point(16, 574);
			this.Platit_RB.Name = "Platit_RB";
			this.Platit_RB.Size = new System.Drawing.Size(83, 26);
			this.Platit_RB.TabIndex = 129;
			this.Platit_RB.TabStop = true;
			this.Platit_RB.Text = "Platit?";
			this.Platit_RB.UseVisualStyleBackColor = true;
			this.Platit_RB.CheckedChanged += new System.EventHandler(this.Platit_RB_CheckedChanged);
			// 
			// Total_TB
			// 
			this.Total_TB.Location = new System.Drawing.Point(79, 477);
			this.Total_TB.Name = "Total_TB";
			this.Total_TB.ReadOnly = true;
			this.Total_TB.Size = new System.Drawing.Size(126, 31);
			this.Total_TB.TabIndex = 128;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label2.Location = new System.Drawing.Point(10, 477);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(63, 36);
			this.label2.TabIndex = 127;
			this.label2.Text = "Total:";
			// 
			// Tratament_TB
			// 
			this.Tratament_TB.Location = new System.Drawing.Point(16, 325);
			this.Tratament_TB.Name = "Tratament_TB";
			this.Tratament_TB.ReadOnly = true;
			this.Tratament_TB.Size = new System.Drawing.Size(400, 134);
			this.Tratament_TB.TabIndex = 126;
			this.Tratament_TB.Text = "";
			// 
			// Dinti_TB
			// 
			this.Dinti_TB.Location = new System.Drawing.Point(292, 136);
			this.Dinti_TB.Name = "Dinti_TB";
			this.Dinti_TB.ReadOnly = true;
			this.Dinti_TB.Size = new System.Drawing.Size(124, 134);
			this.Dinti_TB.TabIndex = 125;
			this.Dinti_TB.Text = "";
			// 
			// Ora_TextBox
			// 
			this.Ora_TextBox.Location = new System.Drawing.Point(172, 63);
			this.Ora_TextBox.Name = "Ora_TextBox";
			this.Ora_TextBox.ReadOnly = true;
			this.Ora_TextBox.Size = new System.Drawing.Size(223, 31);
			this.Ora_TextBox.TabIndex = 124;
			// 
			// Data_TextBox
			// 
			this.Data_TextBox.Location = new System.Drawing.Point(181, 27);
			this.Data_TextBox.Name = "Data_TextBox";
			this.Data_TextBox.ReadOnly = true;
			this.Data_TextBox.Size = new System.Drawing.Size(214, 31);
			this.Data_TextBox.TabIndex = 123;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label1.Location = new System.Drawing.Point(6, 63);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(160, 36);
			this.label1.TabIndex = 122;
			this.label1.Text = "Ora programarii:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label6.Location = new System.Drawing.Point(286, 99);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(130, 36);
			this.label6.TabIndex = 121;
			this.label6.Text = "Dinti afectati:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label5.Location = new System.Drawing.Point(6, 27);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(169, 36);
			this.label5.TabIndex = 108;
			this.label5.Text = "Data programarii:";
			// 
			// Diagnostic_TB
			// 
			this.Diagnostic_TB.Location = new System.Drawing.Point(12, 136);
			this.Diagnostic_TB.Name = "Diagnostic_TB";
			this.Diagnostic_TB.ReadOnly = true;
			this.Diagnostic_TB.Size = new System.Drawing.Size(268, 134);
			this.Diagnostic_TB.TabIndex = 15;
			this.Diagnostic_TB.Text = "";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label9.Location = new System.Drawing.Point(10, 286);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(108, 36);
			this.label9.TabIndex = 110;
			this.label9.Text = "Tratament:";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Myanmar Text", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label10.Location = new System.Drawing.Point(6, 99);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(112, 36);
			this.label10.TabIndex = 109;
			this.label10.Text = "Diagnostic:";
			// 
			// buttonsPanel
			// 
			this.buttonsPanel.AutoScroll = true;
			this.buttonsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.buttonsPanel.Location = new System.Drawing.Point(6, 30);
			this.buttonsPanel.Name = "buttonsPanel";
			this.buttonsPanel.Size = new System.Drawing.Size(426, 595);
			this.buttonsPanel.TabIndex = 132;
			// 
			// groupBox1
			// 
			this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.groupBox1.Controls.Add(this.buttonsPanel);
			this.groupBox1.Font = new System.Drawing.Font("Rockwell", 15F);
			this.groupBox1.ForeColor = System.Drawing.Color.DarkGreen;
			this.groupBox1.Location = new System.Drawing.Point(456, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(438, 631);
			this.groupBox1.TabIndex = 132;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Radiografii";
			// 
			// Salveaza_Button
			// 
			this.Salveaza_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Salveaza_Button.Font = new System.Drawing.Font("Rockwell", 25F);
			this.Salveaza_Button.Location = new System.Drawing.Point(365, 669);
			this.Salveaza_Button.Name = "Salveaza_Button";
			this.Salveaza_Button.Size = new System.Drawing.Size(169, 66);
			this.Salveaza_Button.TabIndex = 133;
			this.Salveaza_Button.Text = "Salveaza";
			this.Salveaza_Button.UseVisualStyleBackColor = true;
			this.Salveaza_Button.Click += new System.EventHandler(this.Salveaza_Button_Click);
			// 
			// VeziProgramare
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(907, 747);
			this.Controls.Add(this.Salveaza_Button);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.Remediu_GB);
			this.Name = "VeziProgramare";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Detalii Programare";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VeziProgramare_FormClosing);
			this.Remediu_GB.ResumeLayout(false);
			this.Remediu_GB.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox Remediu_GB;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.RichTextBox Diagnostic_TB;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox Ora_TextBox;
		private System.Windows.Forms.TextBox Data_TextBox;
		private System.Windows.Forms.RichTextBox Dinti_TB;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.RichTextBox Tratament_TB;
		private System.Windows.Forms.TextBox Total_TB;
		private System.Windows.Forms.RadioButton Platit_RB;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.DateTimePicker DataPlatit;
		private System.Windows.Forms.FlowLayoutPanel buttonsPanel;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button Salveaza_Button;
		private System.Windows.Forms.ComboBox PacientPrezentat_CB;
		private System.Windows.Forms.Label label4;
	}
}
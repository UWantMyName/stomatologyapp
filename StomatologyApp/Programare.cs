﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StomatologyApp
{
	public class Programare
	{
		private Date DataProgramat { get; set; }
		private Time OraProgramat { get; set; }
		private string Diagnostic { get; set; }
		private List<string> DintiAfectati { get; set; }
		private List<string> TratamenteNume { get; set; }
		private List<int> TratamentePreturi { get; set; }
		private ListaRadiografii Radiografii { get; set; }
		private bool Platit { get; set; }
		private Date Data_Platit { get; set; }
		private string Pacient_Prezentat { get; set; }


		public Programare()
		{
			this.Pacient_Prezentat = "neprecizat";
		}

		#region Constructors
		public void SetDataProgramat(DateTime date)
		{
			Date dt = new Date();
			dt.SetZi(date.Day);
			dt.SetLuna(date.Month);
			dt.SetAn(date.Year);

			this.DataProgramat = dt;
		}
		public void SetDataProgramat(Date dt)
		{
			this.DataProgramat = dt;
		}
		public Date GetDataProgramat() { return this.DataProgramat; }

		public void SetOraProgramat(string hour, string minute) { this.OraProgramat = new Time(hour, minute); }
		public Time GetOraProgramat() { return this.OraProgramat; }

		public void SetDiagnostic(string diagnostic) { this.Diagnostic = diagnostic; }
		public string GetDiagnostic() { return this.Diagnostic; }

		public void SetDintiAfectati(List<string> da) { this.DintiAfectati = da; }
		public List<string> GetDintiAfectati() { return this.DintiAfectati; }

		public void SetTratamente(List<string> tratamenteNume, List<int> tratamentePreturi) 
		{
			this.TratamenteNume = tratamenteNume;
			this.TratamentePreturi = tratamentePreturi;
		}
		public List<string> GetTratamenteNume() { return this.TratamenteNume; }
		public List<int> GetTratamentePreturi() { return this.TratamentePreturi; }

		public void SetPlatit(bool platit) { this.Platit = platit; }
		public bool GetPlatit() { return this.Platit; }

		public void SetDataPlatit(Date data_platit) { this.Data_Platit = data_platit; }
		public void SetDataPlatit(DateTime data_platit)
		{
			Date dt = new Date();
			dt.SetZi(data_platit.Day);
			dt.SetLuna(data_platit.Month);
			dt.SetAn(data_platit.Year);
			this.Data_Platit = dt;
		}
		public Date GetDataPlatit() { return this.Data_Platit; }

		public void SetPrezentat(string prezentat) { this.Pacient_Prezentat = prezentat; }
		public string GetPrezentat() { return this.Pacient_Prezentat; }
		#endregion

		public void SetRadiografii(List<Radiografie> lista)
		{
			ListaRadiografii lr = new ListaRadiografii();
			lr.SetRadiografii(lista);
			Radiografii = lr;
		}

		public ListaRadiografii GetRadiografii() { return this.Radiografii; }
	}
}

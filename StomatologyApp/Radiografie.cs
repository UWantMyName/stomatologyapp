﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StomatologyApp
{
	public class Radiografie
	{
		private string name;
		private Image radiografie;
		private Date dataEfectuata;
		private bool empty = false;

		public Radiografie()
		{
			empty = true;
		}

		/// <summary>
		/// Main constructor for Radiografie.
		/// </summary>
		public Radiografie(string name, Image img)
		{
			this.name = name;
			this.radiografie = img;
			dataEfectuata = new Date();
		}

		/// <summary>
		///	Setter for name of radiografie.
		/// </summary>
		/// <param name="name"></param>
		public void SetName(string name) { this.name = name; }

		/// <summary>
		/// Return the name of the radiografie.
		/// </summary>
		/// <returns></returns>
		public string GetName() { return this.name; }

		/// <summary>
		/// Setter for radiografie.
		/// </summary>
		/// <param name="image"></param>
		public void SetRadiografie(Image image) { this.radiografie = image; }

		/// <summary>
		/// Getter for Image of radiografie.
		/// </summary>
		/// <returns></returns>
		public Image GetRadiografie() { return this.radiografie; }

		/// <summary>
		/// Sets the date when the image was taken.
		/// </summary>
		/// <param name="date"></param>
		public void SetDataEfectuata(Date date) { this.dataEfectuata = date; }

		/// <summary>
		/// Returns the date when the image was taken.
		/// </summary>
		/// <returns></returns>
		public Date GetDataEfectuata() { return this.dataEfectuata; }

		public bool IsEmpty()
		{
			return this.empty;
		}
	}
}
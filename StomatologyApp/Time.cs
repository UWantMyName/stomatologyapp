﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StomatologyApp
{
	public class Time
	{
		private int Hour;
		private int Minute;


		public void SetHour(int hour) { this.Hour = hour; }
		public void SetMinute(int minute) { this.Minute = minute; }
		public int GetHour() { return this.Hour; }
		public int GetMinute() { return this.Minute; }

		public Time(string hour, string minute)
		{
			this.Hour = int.Parse(hour);
			this.Minute = int.Parse(minute);
		}

		public Time(int hour, int minute)
		{
			this.Hour = hour;
			this.Minute = minute;
		}

		public override string ToString()
		{
			string h = "";
			string m = "";

			if (Hour < 10) h = "0" + Hour.ToString();
			else h = Hour.ToString();

			if (Minute < 10) m = "0" + Minute.ToString();
			else m = Minute.ToString();


			return h + "h" + m + "m";
		}
	}
}

﻿namespace StomatologyApp
{
	partial class AlegeAtribute
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlegeAtribute));
			this.Nume_CB = new System.Windows.Forms.CheckBox();
			this.Prenume_CB = new System.Windows.Forms.CheckBox();
			this.CNP_CB = new System.Windows.Forms.CheckBox();
			this.NrTelefon_CB = new System.Windows.Forms.CheckBox();
			this.StradaDomiciliu_CB = new System.Windows.Forms.CheckBox();
			this.BlocDomiciliu_CB = new System.Windows.Forms.CheckBox();
			this.Localitatea_CB = new System.Windows.Forms.CheckBox();
			this.Judetul_CB = new System.Windows.Forms.CheckBox();
			this.ProgramatLa_CB = new System.Windows.Forms.CheckBox();
			this.EfectuatLa_CB = new System.Windows.Forms.CheckBox();
			this.Diagnostic_CB = new System.Windows.Forms.CheckBox();
			this.Tratament_CB = new System.Windows.Forms.CheckBox();
			this.Cost_CB = new System.Windows.Forms.CheckBox();
			this.Plata_CB = new System.Windows.Forms.CheckBox();
			this.Toate_Button = new System.Windows.Forms.Button();
			this.OK_Button = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// Nume_CB
			// 
			this.Nume_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.Nume_CB.AutoSize = true;
			this.Nume_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.Nume_CB.Location = new System.Drawing.Point(13, 13);
			this.Nume_CB.Name = "Nume_CB";
			this.Nume_CB.Size = new System.Drawing.Size(102, 42);
			this.Nume_CB.TabIndex = 0;
			this.Nume_CB.Tag = "0";
			this.Nume_CB.Text = "Nume";
			this.Nume_CB.UseVisualStyleBackColor = true;
			this.Nume_CB.CheckedChanged += new System.EventHandler(this.Nume_CB_CheckedChanged);
			// 
			// Prenume_CB
			// 
			this.Prenume_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.Prenume_CB.AutoSize = true;
			this.Prenume_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.Prenume_CB.Location = new System.Drawing.Point(13, 61);
			this.Prenume_CB.Name = "Prenume_CB";
			this.Prenume_CB.Size = new System.Drawing.Size(141, 42);
			this.Prenume_CB.TabIndex = 1;
			this.Prenume_CB.Tag = "1";
			this.Prenume_CB.Text = "Prenume";
			this.Prenume_CB.UseVisualStyleBackColor = true;
			this.Prenume_CB.CheckedChanged += new System.EventHandler(this.Prenume_CB_CheckedChanged);
			// 
			// CNP_CB
			// 
			this.CNP_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.CNP_CB.AutoSize = true;
			this.CNP_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.CNP_CB.Location = new System.Drawing.Point(13, 109);
			this.CNP_CB.Name = "CNP_CB";
			this.CNP_CB.Size = new System.Drawing.Size(80, 42);
			this.CNP_CB.TabIndex = 2;
			this.CNP_CB.Tag = "2";
			this.CNP_CB.Text = "CNP";
			this.CNP_CB.UseVisualStyleBackColor = true;
			this.CNP_CB.CheckedChanged += new System.EventHandler(this.CNP_CB_CheckedChanged);
			// 
			// NrTelefon_CB
			// 
			this.NrTelefon_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.NrTelefon_CB.AutoSize = true;
			this.NrTelefon_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.NrTelefon_CB.Location = new System.Drawing.Point(11, 157);
			this.NrTelefon_CB.Name = "NrTelefon_CB";
			this.NrTelefon_CB.Size = new System.Drawing.Size(159, 42);
			this.NrTelefon_CB.TabIndex = 3;
			this.NrTelefon_CB.Tag = "3";
			this.NrTelefon_CB.Text = "NrTelefon";
			this.NrTelefon_CB.UseVisualStyleBackColor = true;
			this.NrTelefon_CB.CheckedChanged += new System.EventHandler(this.NrTelefon_CB_CheckedChanged);
			// 
			// StradaDomiciliu_CB
			// 
			this.StradaDomiciliu_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.StradaDomiciliu_CB.AutoSize = true;
			this.StradaDomiciliu_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.StradaDomiciliu_CB.Location = new System.Drawing.Point(13, 205);
			this.StradaDomiciliu_CB.Name = "StradaDomiciliu_CB";
			this.StradaDomiciliu_CB.Size = new System.Drawing.Size(235, 42);
			this.StradaDomiciliu_CB.TabIndex = 4;
			this.StradaDomiciliu_CB.Tag = "4";
			this.StradaDomiciliu_CB.Text = "StradaDomiciliu";
			this.StradaDomiciliu_CB.UseVisualStyleBackColor = true;
			this.StradaDomiciliu_CB.CheckedChanged += new System.EventHandler(this.StradaDomiciliu_CB_CheckedChanged);
			// 
			// BlocDomiciliu_CB
			// 
			this.BlocDomiciliu_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.BlocDomiciliu_CB.AutoSize = true;
			this.BlocDomiciliu_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.BlocDomiciliu_CB.Location = new System.Drawing.Point(13, 253);
			this.BlocDomiciliu_CB.Name = "BlocDomiciliu_CB";
			this.BlocDomiciliu_CB.Size = new System.Drawing.Size(205, 42);
			this.BlocDomiciliu_CB.TabIndex = 5;
			this.BlocDomiciliu_CB.Tag = "5";
			this.BlocDomiciliu_CB.Text = "BlocDomiciliu";
			this.BlocDomiciliu_CB.UseVisualStyleBackColor = true;
			this.BlocDomiciliu_CB.CheckedChanged += new System.EventHandler(this.BlocDomiciliu_CB_CheckedChanged);
			// 
			// Localitatea_CB
			// 
			this.Localitatea_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.Localitatea_CB.AutoSize = true;
			this.Localitatea_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.Localitatea_CB.Location = new System.Drawing.Point(13, 301);
			this.Localitatea_CB.Name = "Localitatea_CB";
			this.Localitatea_CB.Size = new System.Drawing.Size(168, 42);
			this.Localitatea_CB.TabIndex = 6;
			this.Localitatea_CB.Tag = "6";
			this.Localitatea_CB.Text = "Localitatea";
			this.Localitatea_CB.UseVisualStyleBackColor = true;
			this.Localitatea_CB.CheckedChanged += new System.EventHandler(this.Localitatea_CB_CheckedChanged);
			// 
			// Judetul_CB
			// 
			this.Judetul_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.Judetul_CB.AutoSize = true;
			this.Judetul_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.Judetul_CB.Location = new System.Drawing.Point(13, 349);
			this.Judetul_CB.Name = "Judetul_CB";
			this.Judetul_CB.Size = new System.Drawing.Size(118, 42);
			this.Judetul_CB.TabIndex = 7;
			this.Judetul_CB.Tag = "7";
			this.Judetul_CB.Text = "Judetul";
			this.Judetul_CB.UseVisualStyleBackColor = true;
			this.Judetul_CB.CheckedChanged += new System.EventHandler(this.Judetul_CB_CheckedChanged);
			// 
			// ProgramatLa_CB
			// 
			this.ProgramatLa_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.ProgramatLa_CB.AutoSize = true;
			this.ProgramatLa_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.ProgramatLa_CB.Location = new System.Drawing.Point(12, 397);
			this.ProgramatLa_CB.Name = "ProgramatLa_CB";
			this.ProgramatLa_CB.Size = new System.Drawing.Size(192, 42);
			this.ProgramatLa_CB.TabIndex = 8;
			this.ProgramatLa_CB.Tag = "8";
			this.ProgramatLa_CB.Text = "ProgramatLa";
			this.ProgramatLa_CB.UseVisualStyleBackColor = true;
			this.ProgramatLa_CB.CheckedChanged += new System.EventHandler(this.ProgramatLa_CB_CheckedChanged);
			// 
			// EfectuatLa_CB
			// 
			this.EfectuatLa_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.EfectuatLa_CB.AutoSize = true;
			this.EfectuatLa_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.EfectuatLa_CB.Location = new System.Drawing.Point(12, 445);
			this.EfectuatLa_CB.Name = "EfectuatLa_CB";
			this.EfectuatLa_CB.Size = new System.Drawing.Size(160, 42);
			this.EfectuatLa_CB.TabIndex = 9;
			this.EfectuatLa_CB.Tag = "9";
			this.EfectuatLa_CB.Text = "EfectuatLa";
			this.EfectuatLa_CB.UseVisualStyleBackColor = true;
			this.EfectuatLa_CB.CheckedChanged += new System.EventHandler(this.EfectuatLa_CB_CheckedChanged);
			// 
			// Diagnostic_CB
			// 
			this.Diagnostic_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.Diagnostic_CB.AutoSize = true;
			this.Diagnostic_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.Diagnostic_CB.Location = new System.Drawing.Point(13, 493);
			this.Diagnostic_CB.Name = "Diagnostic_CB";
			this.Diagnostic_CB.Size = new System.Drawing.Size(167, 42);
			this.Diagnostic_CB.TabIndex = 10;
			this.Diagnostic_CB.Tag = "10";
			this.Diagnostic_CB.Text = "Diagnostic";
			this.Diagnostic_CB.UseVisualStyleBackColor = true;
			this.Diagnostic_CB.CheckedChanged += new System.EventHandler(this.Diagnostic_CB_CheckedChanged);
			// 
			// Tratament_CB
			// 
			this.Tratament_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.Tratament_CB.AutoSize = true;
			this.Tratament_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.Tratament_CB.Location = new System.Drawing.Point(13, 541);
			this.Tratament_CB.Name = "Tratament_CB";
			this.Tratament_CB.Size = new System.Drawing.Size(165, 42);
			this.Tratament_CB.TabIndex = 11;
			this.Tratament_CB.Tag = "11";
			this.Tratament_CB.Text = "Tratament";
			this.Tratament_CB.UseVisualStyleBackColor = true;
			this.Tratament_CB.CheckedChanged += new System.EventHandler(this.Tratament_CB_CheckedChanged);
			// 
			// Cost_CB
			// 
			this.Cost_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.Cost_CB.AutoSize = true;
			this.Cost_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.Cost_CB.Location = new System.Drawing.Point(13, 589);
			this.Cost_CB.Name = "Cost_CB";
			this.Cost_CB.Size = new System.Drawing.Size(85, 42);
			this.Cost_CB.TabIndex = 12;
			this.Cost_CB.Tag = "12";
			this.Cost_CB.Text = "Cost";
			this.Cost_CB.UseVisualStyleBackColor = true;
			this.Cost_CB.CheckedChanged += new System.EventHandler(this.Cost_CB_CheckedChanged);
			// 
			// Plata_CB
			// 
			this.Plata_CB.Appearance = System.Windows.Forms.Appearance.Button;
			this.Plata_CB.AutoSize = true;
			this.Plata_CB.Font = new System.Drawing.Font("Lucida Sans", 20F);
			this.Plata_CB.Location = new System.Drawing.Point(13, 637);
			this.Plata_CB.Name = "Plata_CB";
			this.Plata_CB.Size = new System.Drawing.Size(90, 42);
			this.Plata_CB.TabIndex = 13;
			this.Plata_CB.Tag = "13";
			this.Plata_CB.Text = "Plata";
			this.Plata_CB.UseVisualStyleBackColor = true;
			this.Plata_CB.CheckedChanged += new System.EventHandler(this.Plata_CB_CheckedChanged);
			// 
			// Toate_Button
			// 
			this.Toate_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Toate_Button.Font = new System.Drawing.Font("Rockwell", 20F);
			this.Toate_Button.Location = new System.Drawing.Point(140, 701);
			this.Toate_Button.Name = "Toate_Button";
			this.Toate_Button.Size = new System.Drawing.Size(116, 52);
			this.Toate_Button.TabIndex = 15;
			this.Toate_Button.Text = "Toate";
			this.Toate_Button.UseVisualStyleBackColor = true;
			this.Toate_Button.Click += new System.EventHandler(this.Toate_Button_Click);
			// 
			// OK_Button
			// 
			this.OK_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.OK_Button.Font = new System.Drawing.Font("Rockwell", 20F);
			this.OK_Button.Location = new System.Drawing.Point(12, 701);
			this.OK_Button.Name = "OK_Button";
			this.OK_Button.Size = new System.Drawing.Size(116, 52);
			this.OK_Button.TabIndex = 16;
			this.OK_Button.Text = "OK";
			this.OK_Button.UseVisualStyleBackColor = true;
			this.OK_Button.Click += new System.EventHandler(this.OKButton_Click);
			// 
			// AlegeAtribute
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(271, 765);
			this.Controls.Add(this.OK_Button);
			this.Controls.Add(this.Toate_Button);
			this.Controls.Add(this.Plata_CB);
			this.Controls.Add(this.Cost_CB);
			this.Controls.Add(this.Tratament_CB);
			this.Controls.Add(this.Diagnostic_CB);
			this.Controls.Add(this.EfectuatLa_CB);
			this.Controls.Add(this.ProgramatLa_CB);
			this.Controls.Add(this.Judetul_CB);
			this.Controls.Add(this.Localitatea_CB);
			this.Controls.Add(this.BlocDomiciliu_CB);
			this.Controls.Add(this.StradaDomiciliu_CB);
			this.Controls.Add(this.NrTelefon_CB);
			this.Controls.Add(this.CNP_CB);
			this.Controls.Add(this.Prenume_CB);
			this.Controls.Add(this.Nume_CB);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "AlegeAtribute";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "AlegeAtribute";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Button Toate_Button;
		private System.Windows.Forms.Button OK_Button;
		public System.Windows.Forms.CheckBox Nume_CB;
		private System.Windows.Forms.CheckBox Prenume_CB;
		private System.Windows.Forms.CheckBox CNP_CB;
		private System.Windows.Forms.CheckBox NrTelefon_CB;
		private System.Windows.Forms.CheckBox StradaDomiciliu_CB;
		private System.Windows.Forms.CheckBox BlocDomiciliu_CB;
		private System.Windows.Forms.CheckBox Localitatea_CB;
		private System.Windows.Forms.CheckBox Judetul_CB;
		private System.Windows.Forms.CheckBox ProgramatLa_CB;
		private System.Windows.Forms.CheckBox EfectuatLa_CB;
		private System.Windows.Forms.CheckBox Diagnostic_CB;
		private System.Windows.Forms.CheckBox Tratament_CB;
		private System.Windows.Forms.CheckBox Cost_CB;
		private System.Windows.Forms.CheckBox Plata_CB;
	}
}
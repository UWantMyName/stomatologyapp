﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StomatologyApp
{
	public class Remediu
	{
		private string diagnostic;
		private string tratament;
		private float cost;
		private float plata;

		/// <summary>
		/// Main constructor for Remediu.
		/// </summary>
		public Remediu() 
		{
			diagnostic = null;
			tratament = null;
			cost = 0;
			plata = 0;
		}


		/// <summary>
		/// Setter for Diagnostic.
		/// </summary>
		/// <param name="diagnostic"></param>
		public void SetDiagnostic(string diagnostic) { this.diagnostic = diagnostic; }
		
		/// <summary>
		/// Setter for Tratament.
		/// </summary>
		/// <param name="tratament"></param>
		public void SetTratament(string tratament) { this.tratament = tratament; }

		/// <summary>
		/// Setter for Cost.
		/// </summary>
		/// <param name="cost"></param>
		public void SetCost(float cost) { this.cost = cost; }

		/// <summary>
		/// Setter for Plata.
		/// </summary>
		/// <param name="plata"></param>
		public void SetPlata(float plata) { this.plata = plata; }


		/// <summary>
		/// Getter for Diagnostic.
		/// </summary>
		/// <returns></returns>
		public string GetDiagnostic() { return this.diagnostic; }

		/// <summary>
		/// Getter for Tratament.
		/// </summary>
		/// <returns></returns>
		public string GetTratament() { return this.tratament; }

		/// <summary>
		/// Getter for cost.
		/// </summary>
		/// <returns></returns>
		public float GetCost() { return this.cost; }

		/// <summary>
		/// Getter for plata.
		/// </summary>
		/// <returns></returns>
		public float GetPlata() { return this.plata; }
	}
}

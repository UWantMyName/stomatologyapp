﻿namespace StomatologyApp
{
	public class DetaliiPersonale
	{
		private string nume;
		private string prenume;
		private string cnp;
		private string nrTelefon;
		
		
		/// <summary>
		/// Main constructor for Detalii Personale
		/// </summary>
		public DetaliiPersonale() { }

		/// <summary>
		/// Setter for nume.
		/// </summary>
		/// <param name="nume"> Nume of pacient </param>
		public void SetNume(string nume) { this.nume = nume; }

		/// <summary>
		/// Setter for prenume.
		/// </summary>
		/// <param name="prenume"> Prenume of pacient </param>
		public void SetPrenume(string prenume) { this.prenume = prenume; }

		/// <summary>
		/// Setter for cnp.
		/// </summary>
		/// <param name="cnp"> CNP of pacient </param>
		public void SetCNP(string cnp) { this.cnp = cnp; }

		/// <summary>
		/// Setter for nrTelefon.
		/// </summary>
		/// <param name="nrTelefon"> NrTelefon of pacient </param>
		public void SetNrTelefon(string nrTelefon) { this.nrTelefon = nrTelefon; }



		/// <summary>
		/// Getter for nume.
		/// </summary>
		/// <returns> The nume of pacient </returns>
		public string GetNume() { return this.nume; }

		/// <summary>
		/// Getter for prenume.
		/// </summary>
		/// <returns> The prenume of pacient </returns>
		public string GetPrenume() { return this.prenume; }

		/// <summary>
		/// Getter for cnp.
		/// </summary>
		/// <returns> The CNP of pacient.</returns>
		public string GetCNP() { return this.cnp; }

		/// <summary>
		/// Getter for NrTelefon.
		/// </summary>
		/// <returns> The NrTelefon of Pacient. </returns>
		public string GetNrTelefon() { return this.nrTelefon; }



		/// <summary>
		/// String representation of DetaliiPersonale
		/// </summary>
		/// <returns> String representation of Detalii Personale </returns>
		public override string ToString()
		{
			return this.nume + " " + this.prenume + " " + this.cnp + " " + this.nrTelefon;
		}
	}
}

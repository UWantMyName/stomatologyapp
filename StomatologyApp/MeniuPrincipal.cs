﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace StomatologyApp
{
	public partial class MeniuPrincipal : Form
	{
		public MeniuPrincipal()
		{
			InitializeComponent();

			CheckForFile();
			Tratamente.InitPreturi();
		}

		public MeniuPrincipal(List<string> pathsPacientiProgramareToday)
		{
			InitializeComponent();
			CheckForFile();
			Tratamente.InitPreturi();
		}

		private void CheckForFile()
		{
			if (!File.Exists("RB.stapp"))
			{
				File.Create("RB.stapp");
				using (StreamWriter write = new StreamWriter("RB.stapp"))
				{
					for (int i = 0; i < 3; i++)
						write.WriteLine("1");
					for (int i = 0; i < 10; i++)
						write.WriteLine("0");
					write.Dispose();
				}
			}
		}

		private void NouaRadiografie_Click(object sender, EventArgs e)
		{
			NouaProgramare form = new NouaProgramare();
			form.Show();
		}

		private void VeziDB_Click(object sender, EventArgs e)
		{
			VeziDB f = new VeziDB();
			f.Show();
		}

		private void VeziPacient_Click(object sender, EventArgs e)
		{
			ListaPacienti f = new ListaPacienti();
			f.Show();
		}

		private void SchimbaCalea_Click(object sender, EventArgs e)
		{
			string path = "";
			if (BrowseDialog.ShowDialog() == DialogResult.OK)
			{
				path = BrowseDialog.SelectedPath;
				string dir_path = Directory.GetCurrentDirectory();

				using (StreamWriter write = new StreamWriter(dir_path + "/path.stapp"))
				{
					write.WriteLine(path);
					write.Close();
				}
			}
		}

		private void VeziPacientiAzi_Click(object sender, EventArgs e)
		{
			ProgramariForToday f = new ProgramariForToday();
			f.Show();
		}
	}
}

﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StomatologyApp
{
	public static class DataManagement
	{
		[Obsolete]
		private static void DataFacturare(XGraphics gfx)
		{
			XFont font = new XFont("Verdana", 8, XFontStyle.Regular);
			DateTime curr_date = DateTime.Now;
			

			gfx.DrawString(curr_date.ToString(), font, XBrushes.Black, new XRect(10, 10, 100, 10), XStringFormat.Center);
		}

		[Obsolete]
		private static void Titlu(XGraphics gfx, PdfPage page, string title)
		{
			XFont font = new XFont("Calibri Light", 34, XFontStyle.Regular);

			gfx.DrawString(title, font, XBrushes.Black, new XRect(0, 60, page.Width, 50), XStringFormat.Center);
		}

		[Obsolete]
		private static void DetaliiPacient(XGraphics gfx, Pacient pacient)
		{
			XFont font = new XFont("Verdana", 16, XFontStyle.Underline);
			XFont font1 = new XFont("Verdana", 12, XFontStyle.Regular);

			gfx.DrawString("Detaliile Pacientului:", font, XBrushes.Black, new XRect(50, 125, 100, 50), XStringFormats.CenterLeft);
			gfx.DrawString("Nume: " + pacient.GetNume(), font1, XBrushes.Black, new XRect(70, 150, 200, 50), XStringFormats.CenterLeft);
			gfx.DrawString("Prenume: " + pacient.GetPrenume(), font1, XBrushes.Black, new XRect(70, 170, 200, 50), XStringFormats.CenterLeft);
			gfx.DrawString("CNP: " + pacient.GetCNP(), font1, XBrushes.Black, new XRect(70, 190, 200, 50), XStringFormats.CenterLeft);

			string email = "";
			if (pacient.GetEmail().Length != 0) email = pacient.GetEmail();
			else email = "(nu este specificat)";

			gfx.DrawString("Adresa email: " + email, font1, XBrushes.Black, new XRect(70, 210, 200, 50), XStringFormats.CenterLeft);
			gfx.DrawString("Numar telefon: " + pacient.GetNrTelefon(), font1, XBrushes.Black, new XRect(70, 230, 200, 50), XStringFormats.CenterLeft);
			gfx.DrawString("Sex: " + pacient.GetSex(), font1, XBrushes.Black, new XRect(70, 250, 200, 50), XStringFormats.CenterLeft);
		}

		[Obsolete]
		private static void DetaliiAdresaDomiciliu(XGraphics gfx, PdfPage page, Pacient pacient)
		{
			XFont font = new XFont("Verdana", 16, XFontStyle.Underline);
			XFont font1 = new XFont("Verdana", 12, XFontStyle.Regular);

			gfx.DrawString("Adresa Domiciliu:", font, XBrushes.Black, new XRect(50, 270, 100, 50), XStringFormats.CenterLeft);
			string[] parts = pacient.GetAdresa().Split('\n');
			int y = 290;
			foreach(string s in parts)
			{
				string st = s.Substring(0, s.Length - 1);
				gfx.DrawString(st, font1, XBrushes.Black, new XRect(70, y, 200, 50), XStringFormats.CenterLeft);
				y += 15;
			}
			gfx.DrawString("Localitatea: " + pacient.GetLocaliltate(), font1, XBrushes.Black, new XRect(70, 370, 200, 50), XStringFormats.CenterLeft);
			gfx.DrawString("Judetul: " + pacient.GetJudetul(), font1, XBrushes.Black, new XRect(70, 385, 200, 50), XStringFormats.CenterLeft);
		}

		private static void ListeazaProgramari(XGraphics gfx, PdfPage page1, Programare prog)
		{
			XFont font = new XFont("Verdana", 16, XFontStyle.Underline);
			XFont font1 = new XFont("Verdana", 12, XFontStyle.Regular);
			XPen pen = new XPen(XColors.Black, 2f);

			// ------------------------------------------- Data si ora programarii -------------------------------------------------------------
			gfx.DrawString("Data si ora programarii:", font, XBrushes.Black, new XRect(50, 100, page1.Width, 50), XStringFormats.CenterLeft);
			gfx.DrawString(prog.GetDataProgramat().ToString() + " " + prog.GetOraProgramat().ToString(), font1, XBrushes.Black, new XRect(250, 100, page1.Width, 50), XStringFormats.CenterLeft);

			// ------------------------------------------- Diagnostic -------------------------------------------------------------
			gfx.DrawString("Diagnostic:", font, XBrushes.Black, new XRect(50, 140, 100, 50), XStringFormats.CenterLeft);
			gfx.DrawString(prog.GetDiagnostic(), font1, XBrushes.Black, new XRect(70, 160, 200, 50), XStringFormats.CenterLeft);

			// ------------------------------------------- Dinti afectati -------------------------------------------------------------
			gfx.DrawString("Dinti afectati: ", font, XBrushes.Black, new XRect(50, 210, 100, 50), XStringFormats.CenterLeft);
			List<string> da = prog.GetDintiAfectati();
			for (int i = 0; i < da.Count; i++)
			{
				gfx.DrawString(da[i], font1, XBrushes.Black, new XRect(70, 225 + (i * 15), 100, 50), XStringFormats.CenterLeft);
			}

			// ------------------------------------------- Tratamente -------------------------------------------------------------
			gfx.DrawString("Tratamente: ", font, XBrushes.Black, new XRect(50, 370, 200, 50), XStringFormats.CenterLeft);

			List<string> tratamenteNume = prog.GetTratamenteNume();
			List<int> tratamentePreturi = prog.GetTratamentePreturi();
			int y = 390;
			int plata = 0;
			for (int i = 0; i < tratamenteNume.Count; i++)
			{
				gfx.DrawString(tratamenteNume[i] + " -- " + tratamentePreturi[i].ToString(), font1, XBrushes.Black, new XRect(70, y, 100, 50), XStringFormats.CenterLeft);
				y += 15;
				plata += tratamentePreturi[i];
			}

			// ------------------------------------------- Total de plata -------------------------------------------------------------
			gfx.DrawString("Total de plata:", font, XBrushes.Black, new XRect(50, 450, 200, 50), XStringFormats.CenterLeft);
			gfx.DrawString(plata.ToString() + " RON", font1, XBrushes.Black, new XRect(180, 450, 100, 50), XStringFormats.CenterLeft);
		
			if (prog.GetPlatit())
			{
				gfx.DrawString("Platit: Da", font1, XBrushes.Black, new XRect(50, 470, 100, 50), XStringFormats.CenterLeft);
				gfx.DrawString("Data platii: " + prog.GetDataPlatit(), font1, XBrushes.Black, new XRect(50, 490, 100, 50), XStringFormats.CenterLeft);
			}
			else gfx.DrawString("Platit: Nu", font1, XBrushes.Black, new XRect(50, 470, 100, 50), XStringFormats.CenterLeft);

			gfx.DrawString("S-a prezentat pacientul?   " + prog.GetPrezentat(), font1, XBrushes.Black, new XRect(50, 510, 100, 50), XStringFormats.CenterLeft);
		}

		[Obsolete]
		public static void ExportDeviz(string path, string prog_path, Pacient pacient)
		{
			PdfDocument document = new PdfDocument();
			// Create an empty page
			PdfPage page = document.AddPage();
			// Get an XGraphics object for drawing
			XGraphics gfx = XGraphics.FromPdfPage(page);

			DataFacturare(gfx);
			Titlu(gfx, page, "Deviz");
			DetaliiPacient(gfx, pacient);
			DetaliiAdresaDomiciliu(gfx, page, pacient);

			// Save the document...
			string filename = "Deviz.pdf";
			document.Save(path + "/" + filename);

			ExportDevizProg(prog_path, pacient);
		}

		[Obsolete]
		private static void ExportDevizProg(string path, Pacient pacient)
		{
			List<Programare> programari = pacient.GetProgramari();
			int index_programare = 1;
			foreach (Programare prog in programari)
			{
				PdfDocument doc = new PdfDocument();
				PdfPage page1 = doc.AddPage();
				XGraphics gfx1 = XGraphics.FromPdfPage(page1);

				DataFacturare(gfx1);
				Titlu(gfx1, page1, "Deviz pentru programare");
				ListeazaProgramari(gfx1, page1, prog);

				// Save the document...
				string filename = "Programare.pdf";
				doc.Save(path + "/" + filename);

				index_programare++;
			}
		}

		private static void ExportPersonalData(Pacient pacient, string path)
		{
			using (StreamWriter write = new StreamWriter(path + "/DatePersonale.stapp"))
			{
				write.WriteLine("Nume: " + pacient.GetNume());
				write.WriteLine("Prenume: " + pacient.GetPrenume());
				write.WriteLine("CNP: " + pacient.GetCNP());
				write.WriteLine("Numar Telefon: " + pacient.GetNrTelefon());
				write.WriteLine("Adresa email: " + pacient.GetEmail());
				write.WriteLine("Sex: " + pacient.GetSex());
				write.WriteLine();
				write.WriteLine("Strada Adresa Domiciliu: " + pacient.GetAdresa());
				write.WriteLine("Localitatea Domiciliu: " + pacient.GetLocaliltate());
				write.WriteLine("Judetul Domiciliu: " + pacient.GetJudetul());
				write.Close();
			}
		}

		private static void ExportProgramare(Programare p, string path)
		{
			using (StreamWriter write = new StreamWriter(path + "/DateProgramare.stapp"))
			{
				// Writing the data of the programare
				write.WriteLine("Data Programare: " + p.GetDataProgramat().ToString());
				write.WriteLine("Ora Programare: " + p.GetOraProgramat().ToString());
				write.WriteLine("Diagnostic: " + p.GetDiagnostic());
				write.WriteLine("Dinti afectati: ");
				foreach (string s in p.GetDintiAfectati())
					write.Write(s + ", ");
				write.WriteLine();
				write.WriteLine("Tratamente:");

				List<string> tratamenteNume = p.GetTratamenteNume();
				List<int> tratamentePreturi = p.GetTratamentePreturi();
				for (int i = 0; i < tratamenteNume.Count; i++)
					write.WriteLine(tratamenteNume[i] + " --- " + tratamentePreturi[i].ToString());

				if (p.GetPlatit())
				{
					write.WriteLine("Platit: DA");
					write.WriteLine(p.GetDataPlatit().ToString());
				}
				else write.WriteLine("Platit: NU");

				write.WriteLine("S-a prezentat pacientul?   " + p.GetPrezentat());

				Directory.CreateDirectory(path + "/Radiografii");
				List<Radiografie> lista = p.GetRadiografii().GetRadiografii();
				foreach(Radiografie r in lista)
				{
					string filePath = Path.Combine(path + "/Radiografii", r.GetName());
					Bitmap b = new Bitmap(r.GetRadiografie());
					b.Save(filePath, ImageFormat.Jpeg);
				}

				write.Close();
			}
		}

		[Obsolete]
		public static void SalveazaPacient(Pacient pacient, ProgressBar pb)
		{
			// by now, I am assuming that the path I have saved exists
			string pacienti_path = "";
			using (StreamReader read = new StreamReader(Directory.GetCurrentDirectory() + "/path.stapp"))
			{
				pacienti_path = read.ReadLine();
				read.Close();
			}

			// need to check if there is an existing folder inside the path
			string pacientFolderName = pacient.GetNume() + " " + pacient.GetPrenume() + " - " + pacient.GetCNP();

			IEnumerable<string> foldersQuery =
				from str in Directory.GetDirectories(pacienti_path)
				where str.Contains(pacientFolderName)
				select str;

			string pacientFolderPath = "";
			// THIS HAS A BUG
			if (foldersQuery.Count() == 0)
			{
				pacientFolderName = (Directory.GetDirectories(pacienti_path).Length + 1).ToString() + ") " + pacientFolderName;
				pacientFolderPath = Path.Combine(pacienti_path, pacientFolderName);
				Directory.CreateDirectory(pacientFolderPath);
				ExportPersonalData(pacient, pacientFolderPath);
				pb.Value = 5;
			}
			else
			{
				pacientFolderPath = Path.Combine(pacienti_path, foldersQuery.First<string>());
			}

			// Now, I need to check for the programare folder.
			Programare listPr = pacient.GetProgramari()[0];
			Date dt = listPr.GetDataProgramat();
			Time tm = listPr.GetOraProgramat();
			string programarePath = "";

			foldersQuery =
				from str1 in Directory.GetDirectories(pacientFolderPath)
				where str1.Contains(dt.ToString() + " " + tm.ToString())
				select str1;

			if (foldersQuery.Count() == 0)
			{
				programarePath = pacientFolderPath + "/" + (Directory.GetDirectories(pacientFolderPath).Length + 1).ToString() + ") " + dt.ToString() + " " + tm.ToString();
				Directory.CreateDirectory(programarePath);
				ExportProgramare(listPr, programarePath);
				ExportDeviz(pacientFolderPath, programarePath, pacient);
				pb.Value = 10;
				MessageBox.Show("Pacient salvat cu succes!");
			}
			else
			{
				pb.Value = 10;
				MessageBox.Show("Programare deja existenta! Nu s-au efectuat salvari.");
			}
		}
	
		[Obsolete]
		public static void UpdatePlataDeviz(Pacient pacient, Programare programare, string path)
		{
			// the path that I have is the path to the programare.
			// firstly, I will have to replace the DateProgramare.stapp file.
			File.Delete(path + "\\" + "DateProgramare.stapp");
			using (StreamWriter write = new StreamWriter(path + "\\" + "DateProgramare.stapp"))
			{
				// Writing the data of the programare
				write.WriteLine("Data Programare: " + programare.GetDataProgramat().ToString());
				write.WriteLine("Ora Programare: " + programare.GetOraProgramat().ToString());
				write.WriteLine("Diagnostic: " + programare.GetDiagnostic());
				write.WriteLine("Dinti afectati: ");
				foreach (string s in programare.GetDintiAfectati())
					write.Write(s + ", ");
				write.WriteLine();
				write.WriteLine("Tratamente:");

				List<string> tratamenteNume = programare.GetTratamenteNume();
				List<int> tratamentePreturi = programare.GetTratamentePreturi();
				for (int i = 0; i < tratamenteNume.Count; i++)
					write.WriteLine(tratamenteNume[i] + " --- " + tratamentePreturi[i].ToString());

				if (programare.GetPlatit())
				{
					write.WriteLine("Platit: DA");
					write.WriteLine(programare.GetDataPlatit().ToString());
				}
				else write.WriteLine("Platit: NU");

				write.WriteLine("S-a prezentat pacientul?   " + programare.GetPrezentat());
			}

			// now, I need to export the programare file.

			File.Delete("Programare.pdf");

			PdfDocument doc = new PdfDocument();
			PdfPage page1 = doc.AddPage();
			XGraphics gfx1 = XGraphics.FromPdfPage(page1);

			DataFacturare(gfx1);
			Titlu(gfx1, page1, "Deviz pentru programare");
			ListeazaProgramari(gfx1, page1, programare);

			// Save the document...
			string filename = "Programare.pdf";
			doc.Save(path + "/" + filename);
		}

		public static string[] GetAllPacienti()
		{
			// by now, I am assuming that the path I have saved exists
			string path = "";
			using (StreamReader read = new StreamReader(Directory.GetCurrentDirectory() + "/path.stapp"))
			{
				path = read.ReadLine();
				read.Close();
			}

			return Directory.GetDirectories(path);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StomatologyApp
{
	public partial class ListaPacienti : Form
	{
		private string[] folders;

		public ListaPacienti()
		{
			InitializeComponent();
			InitButtons();
		}

		private void InitButtons()
		{
			try
			{
				folders = DataManagement.GetAllPacienti();
				int tb = 0;

				foreach (string s in folders)
				{
					tb++;
					string[] parts = s.Split('\\');
					Button b = new Button();


					b.Name = "Pacient " + tb.ToString();
					b.Cursor = Cursors.Hand;
					b.Font = new Font("Rockwell", 12);
					b.Size = new Size(417, 42);
					b.Text = parts[parts.Length - 1];
					b.TabIndex = tb;
					b.Visible = true;
					b.Click += new EventHandler(PacientButton_Click);

					flowLayoutPanel1.Controls.Add(b);
				}
			}
			catch(DirectoryNotFoundException exc)
			{
				MessageBox.Show("Nu pot accesa calea pentru salvarea datelor! Probabil un hard-disk extern sau stick USB a fost decuplat din greseala?", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
				this.Close();
			}
			catch(Exception e)
			{
				MessageBox.Show("Eroare neasteptata. Raporteaza eroarea urmatoare" + Environment.NewLine + e.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
				this.Close();
			}
		}

		private void PacientButton_Click(object sender, EventArgs e)
		{
			// I need to read the DatePersonale.stapp from the folders[button.tabindex - 1]
			Button b = sender as Button;
			string path = folders[b.TabIndex - 1] + "\\" + "DatePersonale.stapp";
			Pacient p = new Pacient();

			using (StreamReader read = new StreamReader(path))
			{
				// nume
				string line = read.ReadLine();
				string nume = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					nume += line[i];
				p.SetNume(nume);

				// prenume
				line = read.ReadLine();
				string prenume = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					prenume += line[i];
				p.SetPrenume(prenume);

				// CNP
				line = read.ReadLine();
				string CNP = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					CNP += line[i];
				p.SetCNP(CNP);

				// numar telefon
				line = read.ReadLine();
				string nrtelefon = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					nrtelefon += line[i];
				p.SetNrTelefon(nrtelefon);

				// adresa email
				line = read.ReadLine();
				string email = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					email += line[i];
				p.SetEmail(email);

				// sexul
				line = read.ReadLine();
				string sex = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					sex += line[i];
				p.SetSex(sex);

				// skip one line
				read.ReadLine();

				// adresa domiciliu
				line = read.ReadLine();
				string adresa = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					adresa += line[i];

				line = read.ReadLine();
				while (!line.Contains(":"))
				{
					adresa += Environment.NewLine;
					adresa += line;
					line = read.ReadLine();
				}
				p.SetAdresa(adresa);

				// localitate
				// line is already read from the previous section
				string localitate = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					localitate += line[i];
				p.SetLocalitate(localitate);

				// judet
				line = read.ReadLine();
				string judetul = "";
				for (int i = line.IndexOf(':') + 2; i < line.Length; i++)
					judetul += line[i];
				p.SetJudetul(judetul);

				read.Close();
			}


			DetaliiPacientSelectat f = new DetaliiPacientSelectat(p, folders[b.TabIndex - 1]);
			f.Show();
			this.Close();
		}
	}
}

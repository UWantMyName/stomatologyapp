﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StomatologyApp
{
	/// <summary>
	/// Static class that allows the execution of a few queries in the database.
	/// </summary>
	public static class DatabaseController
	{
		//private static readonly string connectionString = BuildConnString();
		//private static int nrLabels = 0;

		//private static string BuildConnString()
		//{
		//	string str = "";

		//	str += @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=";
		//	str += Directory.GetCurrentDirectory();
		//	str += @"\Pacienti_DB.mdf";
		//	str += ";Integrated Security=True";

		//	return str;
		//}

		///// <summary>
		///// Returns the name of the attribute based on the number.
		///// </summary>
		///// <param name="value"> The number of the attribute </param>
		///// <returns> The name of the attribute</returns>
		//private static string GetAttribute(int value)
		//{
		//	if (value == 0) return "Nume";
		//	if (value == 1) return "Prenume";
		//	if (value == 2) return "CNP";
		//	if (value == 3) return "NrTelefon";
		//	if (value == 4) return "StradaDomiciliu";
		//	if (value == 5) return "BlocDomiciliu";
		//	if (value == 6) return "Localitatea";
		//	if (value == 7) return "Judetul";
		//	if (value == 8) return "ProgramatLa";
		//	if (value == 9) return "EfectuatLa";
		//	if (value == 10) return "Diagnostic";
		//	if (value == 11) return "Tratament";
		//	if (value == 12) return "Cost";
		//	if (value == 13) return "Plata";
		//	return "";
		//}

		///// <summary>
		///// Sets a DataGridView to the result of a specific SELECT command using the specified attributes.
		///// </summary>
		///// <param name="dgv"> DataGridView to visualize the result of the query </param>
		//public static void VeziPacienti(DataGridView dgv)
		//{
		//	dgv.DataSource = null;
		//	dgv.Rows.Clear();
		//	dgv.Refresh();

		//	string attributes = "Id, ";
		//	int nrAttributes = 0;

		//	using (StreamReader read = new StreamReader("RB.stapp"))
		//	{
		//		string line;

		//		for (int i = 0; i < 14; i++)
		//		{
		//			line = read.ReadLine();
		//			if (line == "1")
		//			{
		//				string str = GetAttribute(i);
		//				attributes += str + ", ";
		//				nrAttributes++;
		//			}
		//		}
		//		read.Dispose();
		//	}

		//	if (nrAttributes != 0)
		//	{
		//		// eliminate the , and the space left in both strings for the command
		//		attributes = attributes.Substring(0, attributes.Length - 2);
		//		attributes += " ";

		//		string command = "SELECT " + attributes + " FROM Pacient";

		//		using (SqlConnection con = new SqlConnection(connectionString))
		//		{
		//			con.Open();
		//			using (SqlCommand com = new SqlCommand(command, con))
		//			{
		//				using (SqlDataAdapter sda = new SqlDataAdapter(com))
		//				{
		//					using (DataTable dt = new DataTable())
		//					{
		//						sda.Fill(dt);
		//						dgv.DataSource = dt;
		//					}
		//				}
		//			}
		//			con.Close();
		//		}
		//	}
		//	else
		//	{
		//		MessageBox.Show("Selecteaza macar un atribut. Apasa pe \"Alege Atribute\" si alege macar unul.");
		//	}
		//}
		
		///// <summary>
		///// Sets a DataGridView to the result of a query of all the patients in the database.
		///// </summary>
		///// <param name="dgv"> The DataGridView to visualize the result of the query </param>
		//public static void VeziAllPacienti(DataGridView dgv)
		//{
		//	string command = "SELECT * FROM Pacient";

		//	using (SqlConnection con = new SqlConnection(connectionString))
		//	{
		//		con.Open();
		//		using (SqlCommand com = new SqlCommand(command, con))
		//		{
		//			using (SqlDataAdapter sda = new SqlDataAdapter(com))
		//			{
		//				using (DataTable dt = new DataTable())
		//				{
		//					sda.Fill(dt);
		//					dgv.DataSource = dt;
		//				}
		//			}
		//		}
		//		con.Close();
		//	}
		//}

		///// <summary>
		///// Returns a Pacient with the details specified in the database.
		///// </summary>
		///// <param name="id_nr"> The id of the particular Pacient </param>
		///// <returns></returns>
		//public static Pacient GetDetailsOfSinglePacient(int id_nr)
		//{
		//	string command = "select * from Pacient where Id=" + id_nr.ToString();
		//	Pacient p = new Pacient();
		//	DetaliiPersonale dp = new DetaliiPersonale();

		//	using (SqlConnection con = new SqlConnection(connectionString))
		//	{
		//		con.Open();

		//		using (SqlCommand com = new SqlCommand(command, con))
		//		{
		//			using (SqlDataReader reader = com.ExecuteReader())
		//			{
		//				while (reader.Read())
		//				{
		//					dp.SetNume(reader["Nume"].ToString());
		//					dp.SetPrenume(reader["Prenume"].ToString());
		//					dp.SetCNP(reader["CNP"].ToString());
		//					dp.SetNrTelefon(reader["NrTelefon"].ToString());

		//					AdresaDomiciliu ad = new AdresaDomiciliu();
		//					ad.SetStrada(reader["StradaDomiciliu"].ToString());
		//					ad.SetBloc(int.Parse(reader["BlocDomiciliu"].ToString()));
		//					ad.SetLocalitate(reader["Localitatea"].ToString());
		//					ad.SetJudetul(reader["Judetul"].ToString());

		//					Programare pr = new Programare();
		//					pr.SetProgramat(reader["ProgramatLa"].ToString());
		//					pr.SetEfectuat(reader["EfectuatLa"].ToString());

		//					Remediu rm = new Remediu();
		//					rm.SetDiagnostic(reader["Diagnostic"].ToString());
		//					rm.SetTratament(reader["Tratament"].ToString());
		//					rm.SetCost(float.Parse(reader["Cost"].ToString()));
		//					rm.SetPlata(float.Parse(reader["Plata"].ToString()));

		//					p.SetDetaliiPersonale(dp);
		//					p.SetAdresaDomiciliu(ad);
		//					p.SetProgramare(pr);
		//					p.SetRemediu(rm);
		//				}
		//			}
		//		}

		//		string command2 = "select Radiografie, Nume_Rad, Data_Rad from Radiografii where CNP=" + p.GetDetaliiPersonale().GetCNP();
		//		List<Radiografie> list = new List<Radiografie>();

		//		using (SqlCommand com = new SqlCommand(command2, con))
		//		{
		//			using (SqlDataReader reader = com.ExecuteReader())
		//			{
		//				while (reader.Read())
		//				{
		//					Image img = ImageConverter.ByteArrayToImage(reader["Radiografie"] as byte[]);
		//					string nume = reader["Nume_Rad"].ToString();
		//					list.Add(new Radiografie(nume, img));
		//				}
		//			}
		//		}
		//		con.Close();

		//		ListaRadiografii lr = new ListaRadiografii();
		//		lr.SetRadiografii(list);
		//		p.SetListaradiografii(lr);
		//	}
		//	return p;
		//}

		///// <summary>
		///// Adds a pacient using the data specified.
		///// </summary>
		///// <param name="p"></param>
		//public static void AddPacient(Pacient p, ListaRadiografii list, ProgressBar pbar)
		//{
		//	string queryString = "INSERT INTO Pacient (Nume, Prenume, CNP, NrTelefon, StradaDomiciliu, BlocDomiciliu," +
		//		"Localitatea, Judetul, ProgramatLa, EfectuatLa, Diagnostic, Tratament, Cost, Plata) VALUES (@Nume, @Prenume," +
		//		"@CNP, @NrTelefon, @StradaDomiciliu, @BlocDomiciliu, @Localitatea, @Judetul, @ProgramatLa, @EfectuatLa," +
		//		"@Diagnostic, @Tratament, @Cost, @Plata)";

		//	DetaliiPersonale dp = p.GetDetaliiPersonale();
		//	AdresaDomiciliu ad = p.GetAdresaDomiciliu();
		//	Programare pr = p.GetProgramare();
		//	Remediu rm = p.GetRemediu();
		//	ListaRadiografii lr = p.GetListaradiografii();

		//	using (SqlConnection con = new SqlConnection(connectionString))
		//	{
		//		// Inserting into Pacienti
		//		using (SqlCommand cmd = new SqlCommand(queryString, con))
		//		{
		//			cmd.CommandType = CommandType.Text;
		//			pbar.Value = 1; 
		//			cmd.Parameters.AddWithValue("@Nume", dp.GetNume()); 
		//			pbar.Value = 2; 
		//			cmd.Parameters.AddWithValue("@Prenume", dp.GetPrenume());
		//			pbar.Value = 3; 
		//			cmd.Parameters.AddWithValue("@CNP", dp.GetCNP()); 
		//			pbar.Value = 4; 
		//			cmd.Parameters.AddWithValue("@NrTelefon", dp.GetNrTelefon()); 
		//			pbar.Value = 5; 

		//			cmd.Parameters.AddWithValue("@StradaDomiciliu", ad.GetStrada()); 
		//			pbar.Value = 6; 
		//			cmd.Parameters.AddWithValue("@BlocDomiciliu", ad.GetBloc()); 
		//			pbar.Value = 7; 
		//			cmd.Parameters.AddWithValue("@Localitatea", ad.GetLocalitate()); 
		//			pbar.Value = 8; 
		//			cmd.Parameters.AddWithValue("@Judetul", ad.GetJudetul()); 
		//			pbar.Value = 9; 

		//			cmd.Parameters.AddWithValue("@ProgramatLa", pr.GetProgramat().ToString()); 
		//			pbar.Value = 10; 
		//			cmd.Parameters.AddWithValue("@EfectuatLa", ""); 
		//			pbar.Value = 11; 

		//			cmd.Parameters.AddWithValue("@Diagnostic", rm.GetDiagnostic()); 
		//			pbar.Value = 12; 
		//			cmd.Parameters.AddWithValue("@Tratament", rm.GetTratament()); 
		//			pbar.Value = 13; 
		//			cmd.Parameters.AddWithValue("@Cost", rm.GetCost()); 
		//			pbar.Value = 14; 
		//			cmd.Parameters.AddWithValue("@Plata", rm.GetPlata()); 
		//			pbar.Value = 15; 

		//			con.Open();
		//			cmd.ExecuteScalar(); 
		//			pbar.Value = 16;
		//			con.Close();
		//		}
		//	}

		//	AddRadiografii(p, list, pbar);
		//}
	
		///// <summary>
		///// Adds a list of Radiografii in the database.
		///// </summary>
		///// <param name="p"></param>
		///// <param name="list"></param>
		///// <param name="pbar"></param>
		//private static void AddRadiografii(Pacient p, ListaRadiografii list, ProgressBar pbar)
		//{
		//	string queryString = "INSERT INTO Radiografii VALUES (@Nume, @Prenume, @CNP, @Radiografie, @Nume_Rad, @Data_Rad);";
		//	string nume = p.GetDetaliiPersonale().GetNume();
		//	string prenume = p.GetDetaliiPersonale().GetPrenume();
		//	string cnp = p.GetDetaliiPersonale().GetCNP();
		//	List<Radiografie> lista = list.GetLista();

		//	foreach (Radiografie rad in lista)
		//	{
		//		using (SqlConnection con = new SqlConnection(connectionString))
		//		{
		//			using (SqlCommand cmd = new SqlCommand(queryString, con))
		//			{
		//				byte[] arr = ImageConverter.ImageToByteArray(rad.GetRadiografie());
		//				string numeRad = rad.GetName();
		//				string dataRad = rad.GetDataEfectuata().ToString();

		//				cmd.CommandType = CommandType.Text;
		//				cmd.Parameters.AddWithValue("@Nume", nume);
		//				cmd.Parameters.AddWithValue("@Prenume", prenume);
		//				cmd.Parameters.AddWithValue("@CNP", cnp);
		//				cmd.Parameters.AddWithValue("@Radiografie", arr);
		//				cmd.Parameters.AddWithValue("Nume_Rad", numeRad);
		//				cmd.Parameters.AddWithValue("Data_Rad", dataRad);

		//				con.Open();
		//				cmd.ExecuteNonQuery();
		//				pbar.Value = 25;
		//				con.Close();
		//			}
		//		}
		//	}
		//}

		///// <summary>
		///// Updates the Pacient details, including Radiografii.
		///// </summary>
		///// <param name="p"></param>
		///// <param name="id_nr"></param>
		///// <param name="lista"></param>
		//public static void UpdatePacient(Pacient p, int id_nr, List<Radiografie> lista)
		//{
		//	string command = "UPDATE Pacient " +
		//		"SET Nume=@Nume, Prenume=@Prenume, CNP=@CNP, NrTelefon=@NrTelefon," +
		//		"StradaDomiciliu=@StrDom, BlocDomiciliu=@BlDom, Localitatea=@L, Judetul=@J," +
		//		"ProgramatLa=@ProgLa, EfectuatLa=@EfecLa," +
		//		"Diagnostic=@Diag, Tratament=@Tr, Cost=@C, Plata=@P " +
		//		"WHERE Id=@Id";

		//	string command2 = "INSERT INTO Radiografii VALUES (@Nume, " +
		//		"@Prenume, " +
		//		"@CNP," +
		//		"@Radiografie, " +
		//		"@Nume_Rad, " +
		//		"@Data_Rad);";

		//	DetaliiPersonale dp = p.GetDetaliiPersonale();
		//	AdresaDomiciliu ad = p.GetAdresaDomiciliu();
		//	Programare pr = p.GetProgramare();
		//	Remediu rm = p.GetRemediu();

		//	using (SqlConnection con = new SqlConnection(connectionString))
		//	{
		//		using (SqlCommand cmd = new SqlCommand(command, con))
		//		{
		//			cmd.CommandType = CommandType.Text;
		//			cmd.Parameters.AddWithValue("@Nume", dp.GetNume());
		//			cmd.Parameters.AddWithValue("@Prenume", dp.GetPrenume());
		//			cmd.Parameters.AddWithValue("@CNP", dp.GetCNP());
		//			cmd.Parameters.AddWithValue("@NrTelefon", dp.GetNrTelefon());

		//			cmd.Parameters.AddWithValue("@StrDom", ad.GetStrada());
		//			cmd.Parameters.AddWithValue("@BlDom", ad.GetBloc());
		//			cmd.Parameters.AddWithValue("@L", ad.GetLocalitate());
		//			cmd.Parameters.AddWithValue("@J", ad.GetJudetul());

		//			if (pr.GetProgramat() == null) cmd.Parameters.AddWithValue("@ProgLa", "");
		//			else cmd.Parameters.AddWithValue("ProgLa", pr.GetProgramat().ToString());
		//			if (pr.GetEfectuat() == null) cmd.Parameters.AddWithValue("@EfecLa", "");
		//			else cmd.Parameters.AddWithValue("@EfecLa", pr.GetEfectuat().ToString());

		//			cmd.Parameters.AddWithValue("@Diag", rm.GetDiagnostic());
		//			cmd.Parameters.AddWithValue("@Tr", rm.GetTratament());
		//			cmd.Parameters.AddWithValue("@C", rm.GetCost());
		//			cmd.Parameters.AddWithValue("@P", rm.GetPlata());

		//			cmd.Parameters.AddWithValue("@Id", id_nr.ToString());

		//			con.Open();
		//			cmd.ExecuteNonQuery();
		//			con.Close();
		//		}

		//		foreach(Radiografie r in lista)
		//		{
		//			using (SqlCommand cmd = new SqlCommand(command2, con))
		//			{
		//				cmd.CommandType = CommandType.Text;
		//				cmd.Parameters.AddWithValue("@Nume", dp.GetNume());
		//				cmd.Parameters.AddWithValue("@Prenume", dp.GetPrenume());
		//				cmd.Parameters.AddWithValue("@CNP", dp.GetCNP());
		//				cmd.Parameters.AddWithValue("@Radiografie", ImageConverter.ImageToByteArray(r.GetRadiografie()));
		//				cmd.Parameters.AddWithValue("@Nume_Rad", r.GetName());
		//				cmd.Parameters.AddWithValue("@Data_Rad", r.GetDataEfectuata().ToString());

		//				con.Open();
		//				cmd.ExecuteNonQuery();
		//				con.Close();
		//			}
		//		}
		//	}

		//}



		//private static Label CreateLabel(string text)
		//{
		//	Label label = new Label();
		//	nrLabels++;
		//	label.Name = "label" + nrLabels.ToString();
		//	label.AutoSize = true;
		//	label.Font = new Font("Rockwell", 15);
		//	label.Text = nrLabels.ToString() + ") " + text;

		//	return label;
		//}

		///// <summary>
		///// Used to calculate the range for the ProgressBar.
		///// </summary>
		///// <param name="pacientiFilePath"></param>
		///// <param name="radiografiiFilePath"></param>
		///// <param name="pb"></param>
		///// <param name="panel"></param>
		//public static void CalculateProgress(string pacientiFilePath, string radiografiiFilePath, ProgressBar pb, FlowLayoutPanel panel)
		//{
		//	int lineCount = 0;
		//	if (pacientiFilePath != "") lineCount += File.ReadAllLines(pacientiFilePath).Length;
		//	else lineCount += 1;
		//	if (radiografiiFilePath != "") lineCount += File.ReadAllLines(radiografiiFilePath).Length;
		//	else lineCount += 1;

		//	pb.Maximum = lineCount + 8;
		//}

		///// <summary>
		///// Normally used when you setup the app. Constructs the database by adding the pacienti in the database.
		///// </summary>
		///// <param name="pacientiFilePath"></param>
		///// <param name="pb"></param>
		///// <param name="panel"></param>
		//public static void InsertPacienti(string pacientiFilePath, ProgressBar pb, FlowLayoutPanel panel)
		//{
		//	string command = "INSERT INTO Pacient VALUES (@Nume, @Prenume, @CNP, @NrTelefon," +
		//		"@StradaDomiciliu, @BlocDomiciliu, @Localitatea, @Judetul, @ProgramatLa, @EfectuatLa, @Diagnostic," +
		//		"@Tratament, @Cost, @Plata);";

		//	pb.Value++;

		//	string[] lines = File.ReadAllLines(pacientiFilePath);

		//	nrLabels = panel.Controls.Count;

		//	foreach (string s in lines)
		//	{
		//		using (SqlConnection con = new SqlConnection(connectionString))
		//		{
		//			con.Open();
		//			using (SqlCommand cmd = new SqlCommand(command, con))
		//			{
		//				cmd.CommandType = CommandType.Text;
		//				string[] parts = s.Split(',');
		//				cmd.Parameters.AddWithValue("@Nume", parts[1]);
		//				cmd.Parameters.AddWithValue("@Prenume", parts[2]);
		//				cmd.Parameters.AddWithValue("@CNP", parts[3]);
		//				cmd.Parameters.AddWithValue("@NrTelefon", parts[4]);
		//				cmd.Parameters.AddWithValue("@StradaDomiciliu", parts[5]);
		//				cmd.Parameters.AddWithValue("@BlocDomiciliu", parts[6]);
		//				cmd.Parameters.AddWithValue("@Localitatea", parts[7]);
		//				cmd.Parameters.AddWithValue("@Judetul", parts[8]);
		//				cmd.Parameters.AddWithValue("@ProgramatLa", parts[9]);
		//				cmd.Parameters.AddWithValue("@EfectuatLa", parts[10]);
		//				cmd.Parameters.AddWithValue("@Diagnostic", parts[11]);
		//				cmd.Parameters.AddWithValue("@Tratament", parts[12]);
		//				cmd.Parameters.AddWithValue("@Cost", parts[13]);
		//				cmd.Parameters.AddWithValue("@Plata", parts[14]);

		//				try
		//				{
		//					cmd.ExecuteNonQuery();
		//					pb.Value++;
		//					Label lb = CreateLabel("Adaugat pacientul cu ID-ul " + parts[0]);
		//					panel.Controls.Add(lb);
		//					panel.ScrollControlIntoView(lb);
		//				}
		//				catch (Exception e)
		//				{
		//					throw e;
		//				}
		//			}
		//			con.Close();
		//		}
		//	}
		//	nrLabels = panel.Controls.Count;
		//	Label l = CreateLabel("Adaugare pacienti completa.");
		//	pb.Value++;
		//	panel.Controls.Add(l);
		//	panel.ScrollControlIntoView(l);
		//}

		///// <summary>
		///// Normally used when you setup the app, after adding the pacienti. Adds radiografii in the database.
		///// </summary>
		///// <param name="radiografiiFilePath"></param>
		///// <param name="pb"></param>
		///// <param name="panel"></param>
		//public static void InsertRadiografii(string radiografiiFilePath, ProgressBar pb, FlowLayoutPanel panel)
		//{
		//	string command = "INSERT INTO Radiografii VALUES (@Nume, @Prenume, @CNP, @Radiografie, @Nume_Rad, @Data_Rad);";

		//	pb.Value++;

		//	string[] lines = File.ReadAllLines(radiografiiFilePath);

		//	nrLabels = panel.Controls.Count;

		//	foreach (string s in lines)
		//	{
		//		using (SqlConnection con = new SqlConnection(connectionString))
		//		{
		//			con.Open();
		//			using (SqlCommand cmd = new SqlCommand(command, con))
		//			{
		//				cmd.CommandType = CommandType.Text;
		//				string[] parts = s.Split(',');
		//				cmd.Parameters.AddWithValue("@Nume", parts[1]);
		//				cmd.Parameters.AddWithValue("@Prenume", parts[2]);
		//				cmd.Parameters.AddWithValue("@CNP", parts[3]);
		//				cmd.Parameters.AddWithValue("@Radiografie", ImageConverter.ByteArrayToImage(Encoding.ASCII.GetBytes(parts[4])));
		//				cmd.Parameters.AddWithValue("@Nume_Rad", parts[5]);
		//				cmd.Parameters.AddWithValue("@Data_Rad", parts[6]);

		//				try
		//				{
		//					cmd.ExecuteNonQuery();
		//					pb.Value++;
		//					Label lb = CreateLabel("Adaugat radiografia cu ID-ul " + parts[0]);
		//					panel.Controls.Add(lb);
		//					panel.ScrollControlIntoView(lb);
		//				}
		//				catch (Exception e)
		//				{
		//					throw e;
		//				}
		//			}
		//			con.Close();
		//		}
		//	}
		//	nrLabels = panel.Controls.Count;
		//	Label l = CreateLabel("Adaugare radiografii completa.");
		//	pb.Value++;
		//	panel.Controls.Add(l);
		//	panel.ScrollControlIntoView(l);
		//}

		//public static void ExportData(string selectedPath)
		//{
		//	string command1 = "SELECT * FROM Pacient;";
		//	string command2 = "SELECT * FROM Radiografii;";

		//	using (SqlConnection con = new SqlConnection(connectionString))
		//	{
		//		con.Open();

		//		using (SqlCommand com = new SqlCommand(command1, con))
		//		{
		//			using (SqlDataReader reader = com.ExecuteReader())
		//			{
		//				using (StreamWriter write = new StreamWriter(selectedPath + "\\Pacienti.stapp"))
		//				{
		//					while (reader.Read())
		//					{
		//						write.Write(reader["Id"].ToString() + ",");
		//						write.Write(reader["Nume"].ToString() + ",");
		//						write.Write(reader["Prenume"].ToString() + ",");
		//						write.Write(reader["CNP"].ToString() + ",");
		//						write.Write(reader["NrTelefon"].ToString() + ",");
		//						write.Write(reader["StradaDomiciliu"].ToString() + ",");
		//						write.Write(reader["BlocDomiciliu"].ToString() + ",");
		//						write.Write(reader["Localitatea"].ToString() + ",");
		//						write.Write(reader["Judetul"].ToString() + ",");
		//						write.Write(reader["ProgramatLa"].ToString() + ",");
		//						write.Write(reader["EfectuatLa"].ToString() + ",");
		//						write.Write(reader["Diagnostic"].ToString() + ",");
		//						write.Write(reader["Tratament"].ToString() + ",");
		//						write.Write(reader["Cost"].ToString() + ",");
		//						write.Write(reader["Plata"].ToString() + ",");
		//						write.WriteLine();
		//					}
		//				}
		//			}
		//		}

		//		using (SqlCommand com = new SqlCommand(command2, con))
		//		{
		//			using (SqlDataReader reader = com.ExecuteReader())
		//			{
		//				using (StreamWriter write = new StreamWriter(selectedPath + "\\Radiografii.stapp"))
		//				{
		//					while (reader.Read())
		//					{
		//						write.Write(reader["Id"].ToString() + ",");
		//						write.Write(reader["Nume"].ToString() + ",");
		//						write.Write(reader["Prenume"].ToString() + ",");
		//						write.Write(reader["CNP"].ToString() + ",");

		//						string bitString = BitConverter.ToString(reader["Radiografie"] as byte[]);

		//						write.Write(bitString + ",");
		//						write.Write(reader["Nume_Rad"].ToString() + ",");
		//						write.Write(reader["Data_Rad"].ToString() + ",");
		//						write.WriteLine();
		//					}
		//				}
		//			}
		//		}
		//		con.Close();
		//	}
		//}
	}
}
